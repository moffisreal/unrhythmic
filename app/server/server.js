import {Accounts} from "meteor/accounts-base";

Accounts.onCreateUser((options, user) => {

    if (!user.services.facebook) {
        return user;
    }
    user.username = user.services.facebook.name;
    user.emails = [{address: user.services.facebook.email}];

    return user;
});

