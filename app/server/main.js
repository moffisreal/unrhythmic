// Server entry point, imports all server code
import '/imports/startup/server';
import '/imports/startup/both';
import './server.js';

import { Meteor } from 'meteor/meteor';

Meteor.startup(() => {
    // configure facebook
    Meteor.call('facebook.setConf');
    // create first admin user
    Meteor.call('users.firstUserAdmin');
});

