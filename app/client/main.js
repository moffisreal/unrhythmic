// Client entry point, imports all client code

import '/imports/startup/client';
import '/imports/startup/both';

import { Meteor } from 'meteor/meteor';
import React from 'react';
import { render } from 'react-dom';
import Routes from '../imports/startup/client/routes';

Meteor.startup(() => {
    render(
        <Routes/>,
        document.getElementById('app'),
    );
});
