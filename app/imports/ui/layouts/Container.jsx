import React, { Component } from "react";
import Footer from "../components/Block/Footer";
import "./Container.less";
import { Link } from "react-router-dom";

export default class Container extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return <div id="container">
                    <section id="container__top">
                        <section className="logo">
                            <Link to="/">
                                <img src="/img/logo.png" alt="logo"></img>
                            </Link>
                        </section>
                        <section id="content">
                            { this.props.children }
                        </section>
                    </section>
                    <section id="container__bottom">
                        <Footer/>
                    </section>
            </div>
    }
}

