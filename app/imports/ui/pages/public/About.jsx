import React, { Component } from "react/react";
import Public from "../../components/Page/Public";

import './style/About.less';

export default class About extends Component {
    render() {
        return (
            <Public title="Tickets">
                <section id="content-top">
                    <div id="content-top__div">
                        <section className="content-background-top">
                            <article className="about">
                                <div id="unrhythmic-txt">UNRHYTHMIC</div>
                                <br />
                                <p className="txt">
                                    Unrhythmic is a series of events hosting local artists and international friends (or vice-versa).
                                </p>
                                <p className="txt">
                                    We promote groove-driven sounds and rhythms that bring back soul in dance music. The main idea is to offer a space where strangers become friends dancing along to a couple of quality tunes.
                                    Focusing on something in between Philly soul, West African polyrhythms and Chicago house.</p>
                                <p className="txt">
                                    Each time, we will throw a spotlight on a specific scene we love and believe in.</p>
                                <p className="txt">
                                    We are also seeking to establish a "clean and green" dance community by recycling cigarette ends or by providing reusable cups and drink tokens.
                                    Feel free to get in touch if you could help us in any manner about it.
                                </p>
                            </article>
                        </section>
                    </div>
                </section>
                <section id="content-bottom">
                    <div id="content-bottom__div">
                    </div>
                </section>
            </Public>
        );
    }
}
