import React, { Component } from "react/react";
import Public from "../../components/Page/Public";

import './style/Ticket.less';

export default class Ticket extends Component {

    constructor() {
        super();

    }

    static registerClickOnTicketFacebook() {
        fbq('track', 'Purchase', {
            value: 5,
            currency: 'EUR'
        });
    }

    render() {
        return (
            <Public title="Tickets">
                <section id="content-top">
                    <div id="content-top__div">
                        <section className="content-background-top">
                            <a id="ticket-link" target="_blank" href="https://www.residentadvisor.net/events/1285909" onClick={ Ticket.registerClickOnTicketFacebook }>
                                <img id="golden-ticket" src="/img/background/gold-ticket.png" alt="goldenticket"/>
                                <img id="golden-ticket-mobile" src="/img/background/gold-ticket-mobile.png" alt="goldenticket-mobile"/>
                            </a>
                        </section>
                    </div>
                </section>
                <section id="content-bottom">
                    <div id="content-bottom__div">
                    </div>
                </section>
            </Public>
        );
    }
}
