import React, { Component } from "react/react";
import Post from "../../components/Block/Post";
import Public from "../../components/Page/Public";

export default class PostView extends Component {
    render() {
        return (

            <Public title="NEWS">
                <section id="content-top">
                    <div id="content-top__div">
                        <div className="content-top-list">
                            {
                                // show first post (sorted by createdAt in PostListContainer.jsx)
                                this.props.post.map((post, i) => (
                                    <Post key={ i+1 } i={ i+1 } post={ post } showAll={ true } preview={ true } type="full"/>
                                ))
                            }
                        </div>
                    </div>
                </section>
                <section id="content-bottom">
                    <div id="content-bottom__div">
                    </div>
                </section>
            </Public>
        );
    }
}
