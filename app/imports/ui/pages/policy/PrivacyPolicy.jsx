import React, { Component } from "react";
import Public from "../../components/Page/Public";
import './style/PrivacyPolicy.less';

export default class PrivacyPolicy extends Component {

    constructor(props){
        super(props);
    };

    render() {
        return (
            <Public title="Privacy">
                <section id="content-top">
                    <div id="content-top__div">
                        <article className="content-background-top">
                            <div className="privacy-policy" >
                                <p className="p">Effective date: April 16, 2019</p>


                                <p className="p">Collectif Mizo ("us", "we", or "our") operates the https://www.unrhythmic.be website (the "Service").</p>

                                <p className="p">This page informs you of our policies regarding the collection, use, and disclosure of personal data when you use our Service and the choices you have associated with that data. Our Privacy Policy  for Collectif Mizo is created with the help of the <a href="https://www.freeprivacypolicy.com/free-privacy-policy-generator.php">Free Privacy Policy Generator</a>.</p>

                                <p className="p">We use your data to provide and improve the Service. By using the Service, you agree to the collection and use of information in accordance with this policy. Unless otherwise defined in this Privacy Policy, terms used in this Privacy Policy have the same meanings as in our Terms and Conditions, accessible from https://www.unrhythmic.be</p>


                                <h2 >Information Collection And Use</h2>

                                <p className="p">We collect several different types of information for various purposes to provide and improve our Service to you.</p>

                                <h3 className="h3">Types of Data Collected</h3>

                                <h4>Personal Data</h4>

                                <p className="p">While using our Service, we may ask you to provide us with certain personally identifiable information that can be used to contact or identify you ("Personal Data"). Personally identifiable information may include, but is not limited to:</p>

                                <ul className="ul">
                                    <li>Email address</li><li>First name and last name</li><li>Address, State, Province, ZIP/Postal code, City</li><li>Cookies and Usage Data</li>
                                </ul>

                                <h4>Usage Data</h4>

                                <p className="p">We may also collect information how the Service is accessed and used ("Usage Data"). This Usage Data may include information such as your computer's Internet Protocol address (e.g. IP address), browser type, browser version, the pages of our Service that you visit, the time and date of your visit, the time spent on those pages, unique device identifiers and other diagnostic data.</p>

                                <h4>Tracking & Cookies Data</h4>
                                <p className="p">We use cookies and similar tracking technologies to track the activity on our Service and hold certain information.</p>
                                <p className="p">Cookies are files with small amount of data which may include an anonymous unique identifier. Cookies are sent to your browser from a website and stored on your device. Tracking technologies also used are beacons, tags, and scripts to collect and track information and to improve and analyze our Service.</p>
                                <p className="p">You can instruct your browser to refuse all cookies or to indicate when a cookie is being sent. However, if you do not accept cookies, you may not be able to use some portions of our Service.</p>
                                <p className="p">Examples of Cookies we use:</p>
                                <ul className="ul">
                                    <li><strong>Session Cookies.</strong> We use Session Cookies to operate our Service.</li>
                                    <li><strong>Preference Cookies.</strong> We use Preference Cookies to remember your preferences and various settings.</li>
                                    <li><strong>Security Cookies.</strong> We use Security Cookies for security purposes.</li>
                                </ul>

                                <h2 className="h2">Use of Data</h2>

                                <p className="p">Collectif Mizo uses the collected data for various purposes:</p>
                                <ul className="ul">
                                    <li>To provide and maintain the Service</li>
                                    <li>To notify you about changes to our Service</li>
                                    <li>To allow you to participate in interactive features of our Service when you choose to do so</li>
                                    <li>To provide customer care and support</li>
                                    <li>To provide analysis or valuable information so that we can improve the Service</li>
                                    <li>To monitor the usage of the Service</li>
                                    <li>To detect, prevent and address technical issues</li>
                                </ul>

                                <h2 className="h2">Transfer Of Data</h2>
                                <p className="p">Your information, including Personal Data, may be transferred to — and maintained on — computers located outside of your state, province, country or other governmental jurisdiction where the data protection laws may differ than those from your jurisdiction.</p>
                                <p className="p">If you are located outside Belgium and choose to provide information to us, please note that we transfer the data, including Personal Data, to Belgium and process it there.</p>
                                <p className="p">Your consent to this Privacy Policy followed by your submission of such information represents your agreement to that transfer.</p>
                                <p className="p">Collectif Mizo will take all steps reasonably necessary to ensure that your data is treated securely and in accordance with this Privacy Policy and no transfer of your Personal Data will take place to an organization or a country unless there are adequate controls in place including the security of your data and other personal information.</p>

                                <h2 className="h2">Disclosure Of Data</h2>

                                <h3 className="h3">Legal Requirements</h3>
                                <p className="p">Collectif Mizo may disclose your Personal Data in the good faith belief that such action is necessary to:</p>
                                <ul className="ul">
                                    <li>To comply with a legal obligation</li>
                                    <li>To protect and defend the rights or property of Collectif Mizo</li>
                                    <li>To prevent or investigate possible wrongdoing in connection with the Service</li>
                                    <li>To protect the personal safety of users of the Service or the public</li>
                                    <li>To protect against legal liability</li>
                                </ul>

                                <h2 className="h2">Security Of Data</h2>
                                <p className="p">The security of your data is important to us, but remember that no method of transmission over the Internet, or method of electronic storage is 100% secure. While we strive to use commercially acceptable means to protect your Personal Data, we cannot guarantee its absolute security.</p>

                                <h2 className="h2">Service Providers</h2>
                                <p className="p">We may employ third party companies and individuals to facilitate our Service ("Service Providers"), to provide the Service on our behalf, to perform Service-related services or to assist us in analyzing how our Service is used.</p>
                                <p className="p">These third parties have access to your Personal Data only to perform these tasks on our behalf and are obligated not to disclose or use it for any other purpose.</p>

                                <h3 className="h3">Analytics</h3>
                                <p className="p">We may use third-party Service Providers to monitor and analyze the use of our Service.</p>
                                <ul className="ul">
                                    <li>
                                        <p className="small p"><strong>Google Analytics</strong></p>
                                        <p className="small p">Google Analytics is a web analytics service offered by Google that tracks and reports website traffic. Google uses the data collected to track and monitor the use of our Service. This data is shared with other Google services. Google may use the collected data to contextualize and personalize the ads of its own advertising network.</p>
                                        <p className="small p">You can opt-out of having made your activity on the Service available to Google Analytics by installing the Google Analytics opt-out browser add-on. The add-on prevents the Google Analytics JavaScript (ga.js, analytics.js, and dc.js) from sharing information with Google Analytics about visits activity.</p>                <p className="p">For more information on the privacy practices of Google, please visit the Google Privacy & Terms web page: <a href="https://policies.google.com/privacy?hl=en">https://policies.google.com/privacy?hl=en</a></p>
                                    </li>
                                </ul>


                                <h2 className="h2">Links To Other Sites</h2>
                                <p className="p">Our Service may contain links to other sites that are not operated by us. If you click on a third party link, you will be directed to that third party's site. We strongly advise you to review the Privacy Policy of every site you visit.</p>
                                <p className="p">We have no control over and assume no responsibility for the content, privacy policies or practices of any third party sites or services.</p>


                                <h2 className="h2">Children's Privacy</h2>
                                <p className="p">Our Service does not address anyone under the age of 18 ("Children").</p>
                                <p className="p">We do not knowingly collect personally identifiable information from anyone under the age of 18. If you are a parent or guardian and you are aware that your Children has provided us with Personal Data, please contact us. If we become aware that we have collected Personal Data from children without verification of parental consent, we take steps to remove that information from our servers.</p>


                                <h2 className="h2">Changes To This Privacy Policy</h2>
                                <p className="p"> We may update our Privacy Policy from time to time. We will notify you of any changes by posting the new Privacy Policy on this page.</p>
                                <p className="p">We will let you know via email and/or a prominent notice on our Service, prior to the change becoming effective and update the "effective date" at the top of this Privacy Policy.</p>
                                <p className="p">You are advised to review this Privacy Policy periodically for any changes. Changes to this Privacy Policy are effective when they are posted on this page.</p>


                                <h2 className="h2">Contact Us</h2>
                                <p className="p">If you have any questions about this Privacy Policy, please contact us:</p>
                                <ul className="ul">
                                    <li>By email: info[@]unrhythmic.be</li>
                                    <li>By visiting this page on our website: https://www.unrhythmic.be/privacy-policy</li>

                                </ul>
                            </div>

                        </article>
                    </div>
                </section>
                <section id="content-bottom">
                    <div id="content-bottom__div">
                        <div className="content-bottom__article--even">
                        </div>
                    </div>
                </section>
            </Public>
        );
    };
}

/*
*  <section id="content-bottom">

                   <div className="privacy-policy">
                                <h1 className="h2">Politique de Confidentialité</h1>
                                <p className="p">Date de prise d'effet: April 16, 2019</p>
                                <p className="p">Collectif Mizo ("nous", "notre", "nos") exploite le site web https://www.unrhythmic.be (ci-après désigné par le terme "Service").</p>
                                <p className="p">Cette page vous explique nos politiques en matière de collecte, d'utilisation et de communication des données à caractère personnel lorsque vous utilisez notre Service ainsi que les choix qui s'offrent à vous en ce qui concerne ces données. Our Privacy Policy  for Collectif Mizo is created with the help of the <a href="https://www.freeprivacypolicy.com/free-privacy-policy-generator.php">Free Privacy Policy Generator</a>.</p>
                                <p className="p">Nous utilisons vos données pour fournir et améliorer le Service. En utilisant le Service, vous consentez à la collecte et à l'utilisation d'informations conformément à la présente politique. Sauf définition contraire dans la présente Politique de Confidentialité, les termes utilisés dans la présente Politique de Confidentialité ont la même signification que dans nos Conditions Générales qui sont disponibles sur https://www.unrhythmic.be</p>

                                <h2 className="h2">Définitions</h2>
                                <ul className="ul">
                                    <li>
                                        <p className="p"><strong>Service</strong></p>
                                        <p className="p">Par Service on entend le site web https://www.unrhythmic.be exploité par Collectif Mizo</p>
                                    </li>
                                    <li>
                                        <p className="p"><strong>Données à caractère personnel</strong></p>
                                        <p className="p">Données à Caractère Personnel désigne des données concernant un individu vivant qui peut être identifié à partir de ces données (ou à partir de ces données et d'autres informations en notre possession ou susceptibles d'entrer en notre possession).</p>
                                    </li>
                                    <li>
                                        <p className="p"><strong>Données d'Utilisation</strong></p>
                                        <p className="p">Les Données d'Utilisation sont recueillies automatiquement et sont générées soit par l'utilisation du Service, soit par l'infrastructure du Service proprement dite (par exemple, durée de consultation d'une page).</p>
                                    </li>
                                    <li>
                                        <p className="p"><strong>Cookies</strong></p>
                                        <p className="p">Les cookies sont de petits fichiers enregistrés sur votre dispositif (ordinateur ou dispositif mobile).</p>
                                    </li>
                                </ul>

                                <h2 className="h2">Collecte et utilisation des données</h2>
                                <p className="p">Nous recueillons plusieurs types de données à différentes fins en vue de vous fournir notre Service et de l'améliorer.</p>

                                <h3>Types de données recueillies</h3>

                                <h4>Données à Caractère Personnel</h4>
                                <p className="p">Lorsque vous utilisez notre Service, il est possible que nous vous demandions de nous fournir certaines données personnelles nominatives qui peuvent servir à vous contacter ou à vous identifier ("Données à Caractère Personnel"). Les données personnelles nominatives peuvent comprendre, mais de manière non limitative:</p>

                                <ul className="ul">
                                    <li>Adresse e-mail</li>    <li>Prénom et nom de famille</li>        <li>Adresse, ville, province, état, code postal</li>    <li>Cookies et Données d'Utilisation</li>
                                </ul>

                                <h4>Données d'Utilisation</h4>

                                <p className="p">Nous pouvons également recueillir des informations relatives au mode d'accès et d'utilisation du Service ("Données d'Utilisation"). Ces Données d'Utilisation peuvent comprendre des informations telles que l'adresse de protocole Internet (c.-à-d. l'adresse IP) de votre ordinateur, le type de navigateur, la version du navigateur, les pages de notre Service que vous consultez, la date et l'heure de votre visite, le temps passé sur ces pages, les identifiants uniques de dispositifs ainsi que d'autres données de diagnostic.</p>

                                <h4>Suivi et données de cookies</h4>
                                <p className="p">Nous avons recours à des cookies et à d'autres technologies de suivi similaires pour effectuer un suivi des activités effectuées dans notre Service, et nous conservons certaines informations.</p>
                                <p className="p">Les cookies sont des fichiers à faible volume de données pouvant comporter un identifiant unique anonyme. Les cookies sont envoyés à votre navigateur depuis un site web et sont stockés sur votre dispositif. D'autres technologies de suivi telles que les pixels, les balises et les scripts sont également utilisées pour recueillir et suivre des informations et afin d'améliorer et d'analyser notre Service.</p>
                                <p className="p">Vous pouvez demander à votre navigateur de refuser tous les cookies ou de vous avertir lorsqu'un cookie est envoyé. Toutefois, si vous n'acceptez pas les cookies, il se peut que vous ne puissiez pas utiliser certains éléments de notre Service.</p>
                                <p className="p">Exemples de cookies que nous utilisons :</p>
                                <ul className="ul">
                                    <li><strong>Cookies de Session.</strong> Nous utilisons des Cookies de Session pour faire fonctionner notre Service.</li>
                                    <li><strong>Cookies de Préférences.</strong> Nous utilisons des Cookies de Préférences pour mémoriser vos préférences et vos différents paramètres.</li>
                                    <li><strong>Cookies de Sécurité.</strong> Nous utilisons des Cookies de Sécurité pour des raisons de sécurité.</li>
                                </ul>

                                <h2 className="h2">Utilisation des données</h2>

                                <p className="p">Collectif Mizo utilise les données recueillies à des fins diverses:</p>
                                <ul className="ul">
                                    <li>Pour fournir et assurer notre Service</li>
                                    <li>Pour vous faire part des changements apportés à notre Service</li>
                                    <li>Pour vous permettre d'utiliser les fonctions interactives de notre Service quand vous le souhaitez</li>
                                    <li>Pour assurer l'assistance client</li>
                                    <li>Pour recueillir des données précieuses ou d'analyses qui nous permettront d'améliorer notre Service</li>
                                    <li>Pour contrôler l'utilisation de notre Service</li>
                                    <li>Pour détecter, prévenir et régler les problèmes techniques</li>
                                </ul>

                                <h2 className="h2">Transfert des données</h2>
                                <p className="p">Les informations vous concernant, notamment vos Données à Caractère Personnel, peuvent être transférées de votre région, province, pays, ou autre division territoriale vers des ordinateurs – et stockées sur ces derniers – situés à un endroit où la législation relative à la protection des données diffère de celle du territoire où vous résidez.</p>
                                <p className="p">Si vous résidez hors de/du Belgium et que vous choisissez de nous communiquer des informations, sachez que nous transférons les données, y compris les Données à Caractère Personnel, vers le/la Belgium et que nous les y traitons.</p>
                                <p className="p">En acceptant la présente Politique de Confidentialité puis en soumettant ces informations, vous consentez à ce transfert.</p>
                                <p className="p">Collectif Mizo prendra toutes les mesures raisonnablement nécessaires pour faire en sorte que vos données soient traitées de manière sécurisée et conformément à la présente Politique de Confidentialité et vos Données à Caractère Personnel ne seront transférées vers aucune organisation ni aucun pays à moins que des contrôles adéquats ne soient en place, notamment en ce qui concerne la sécurité de vos données et d'autres données personnelles.</p>

                                <h2 className="h2">Communication de données</h2>
                                <h3>Exigences légales</h3>
                                <p className="p">Collectif Mizo peut communiquer vos Données à Caractère Personnel si elle estime de bonne foi que cela est nécessaire pour:</p>
                                <ul className="ul">
                                    <li>S'acquitter d'une obligation légale</li>
                                    <li>Protéger et défendre les droits ou les biens de Collectif Mizo</li>
                                    <li>Prévenir d'éventuels actes répréhensibles ou enquêter sur de tels actes dans le cadre du Service </li>
                                    <li>Assurer la sécurité personnelle des utilisateurs du Service ou du public</li>
                                    <li>Se protéger contre la responsabilité civile</li>
                                </ul>

                                <h2 className="h2">Sécurité des données</h2>
                                <p className="p">La sécurité de vos données nous tient à cœur. Toutefois, n'oubliez pas qu'aucune méthode de transmission de données par Internet ou méthode de stockage électronique n'est sûre à 100 %. Bien que nous nous efforcions d'utiliser des méthodes commercialement acceptables pour protéger vos Données à Caractère Personnel, nous ne pouvons pas leur garantir une sécurité absolue.</p>

                                <h2 className="h2">Prestataires de services</h2>
                                <p className="p">Nous pouvons faire appel à des sociétés tierces et à des tierces personnes pour faciliter la prestation de notre Service ("Prestataires de Services"), assurer le Service en notre nom, assurer des services liés au Service ou nous aider à analyser la façon dont notre Service est utilisé.</p>
                                <p className="p">Ces tiers n'ont accès à vos Données à Caractère Personnel que pour effectuer ces tâches en notre nom et il leur est interdit de les communiquer ou de les utiliser à quelle qu'autre fin. </p>

                                <h3>Analyses</h3>
                                <p className="p">Nous pouvons faire appel à des Prestataires de Services tiers pour surveiller et analyser l'utilisation de notre Service.</p>
                                <ul className="ul">
                                    <li>
                                        <p className="small p"><strong>Google Analytics</strong></p>
                                        <p className="small p">Google Analytics est un service d'analyse web proposé par Google qui assure le suivi du trafic d'un site web et en rend compte. Google utilise les données recueillies pour suivre et surveiller l'utilisation de notre Service. Ces données sont partagées avec d'autres services Google. Google peut utiliser les données recueillies pour contextualiser et personnaliser les annonces de son propre réseau publicitaire.</p>
                                        <p className="small p">Vous pouvez empêcher que vos activités dans le cadre du Service ne soient mises à la disposition de Google Analytics en installant le plug-in pour navigateur Analytics Opt out browser add-on de Google Analytics. Ce plug-in empêche le code JavaScript de Google Analytics JavaScript (ga.js, analytics.js et dc.js) de partager les informations concernant les activités liées aux visites avec Google Analytics. </p>                <p className="p">Pour plus de précisions sur les pratiques de confidentialité de Google, merci de consulter la page web Protection de la vie privée et conditions de Google: <a href="https://policies.google.com/privacy?hl=en">https://policies.google.com/privacy?hl=en</a></p>
                                    </li>
                                </ul>

                                <h2 className="h2">Liens pointant vers d'autres sites</h2>
                                <p className="p">Il se peut que notre Service contienne des liens pointant vers d'autres sites que nous n'exploitons pas. Si vous cliquez sur un lien de tiers, vous serez redirigé vers le site de ce tiers. Nous vous recommandons vivement d'examiner la politique de confidentialité de chacun des sites que vous consultez.</p>
                                <p className="p">Nous n'avons aucun contrôle sur le contenu, les politiques ou pratiques de confidentialité des sites ou services de tiers et déclinons toute responsabilité en ce qui les concerne.</p>

                                <h2 className="h2">Vie privée des enfants</h2>
                                <p className="p">Notre Service ne s'adresse pas aux personnes de moins de 18 ans ("Enfants").</p>
                                <p className="p">Nous ne recueillons pas sciemment de données personnelles nominatives auprès de personnes de moins de 18 ans. Si vous êtes un parent ou un tuteur et que vous savez que votre Enfant nous a communiqué des Données à Caractère Personnel, veuillez nous contacter. Si nous apprenons que nous avons recueilli des Données à Caractère Personnel auprès d'enfants sans vérifier s'il y a consentement parental, nous faisons le nécessaire pour supprimer ces informations de nos serveurs.</p>

                                <h2 className="h2">Modifications de la présente Politique de Confidentialité</h2>
                                <p className="p">Nous nous réservons le droit d'actualiser notre Politique de Confidentialité de temps à autre. Nous vous informerons de toute modification en publiant la nouvelle Politique de Confidentialité sur cette page.</p>
                                <p className="p">Avant que la modification ne prenne effet, nous vous en informerons par e-mail et/ ou en plaçant un avis bien en vue dans notre Service et nous actualiserons la "date de prise d'effet" qui figure en haut de la présente Politique de Confidentialité.</p>
                                <p className="p">Nous vous conseillons de consulter la présente Politique de Confidentialité périodiquement pour prendre connaissance de toute modification. Les modifications apportées à la présente Politique de Confidentialité prennent effet lorsqu'elles sont publiées sur cette page.</p>

                                <h2 className="h2">Nous contacter</h2>
                                <p className="p">Pour toute question relative à la présente Politique de Confidentialité, veuillez nous contacter:</p>
                                <ul className="ul">
                                    <li>Par courrier électronique: info@unrhythmic.be</li>
                                    <li>En consultant cette page sur notre site web: https://www.unrhythmic.be/privacy-policy</li>
                                </ul>
                            </div>
                    <div className="privacy-policy">
                        <h1>Datenschutz-Richtlinie</h1>


                        <p className="p">Datum des Inkrafttretens: April 16, 2019</p>


                        <p className="p">Collectif Mizo ("wir", "uns", "unser" usw.) betreibt die Website https://www.unrhythmic.be (nachstehend als "Dienst" bezeichnet).</p>

                        <p className="p">Diese Seite enthält Informationen zu der Art und Weise, auf welche wir personenbezogene Daten erfassen, nutzen und offenlegen, wenn Sie unseren Dienst nutzen, sowie zu den Optionen, die Ihnen im Zusammenhang mit diesen Daten zur Verfügung stehen. Our Privacy Policy  for Collectif Mizo is created with the help of the <a href="https://www.freeprivacypolicy.com/free-privacy-policy-generator.php">Free Privacy Policy Generator</a>.</p>

                        <p className="p">Wir nutzen Ihre Daten zur Bereitstellung und Verbesserung unseres Dienstes. Durch Inanspruchnahme des Dienstes erklären Sie sich mit der Erfassung und Nutzung von Daten durch uns nach Maßgabe dieser Richtlinie einverstanden. Soweit in dieser Datenschutz-Richtlinie nicht jeweils etwas anderes angegeben ist, kommt den in dieser Datenschutz-Richtlinie vorkommenden Begriffen jeweils dieselbe Bedeutung zu, die diesen in unseren Allgemeinen Geschäftsbedingungen (Terms and Conditions) (abrufbar über die  https://www.unrhythmic.be) zugewiesen wurde.</p>

                        <h2 className="h2">Begriffsbestimmungen</h2>
                        <ul className="ul">
                            <li>
                                <p className="p"><strong>Dienst</strong></p>
                                <p className="p">Der Dienst ist die von dem Collectif Mizo betriebene Website https://www.unrhythmic.be</p>
                            </li>
                            <li>
                                <p className="p"><strong>Personenbezogene Daten</strong></p>
                                <p className="p">Personenbezogene Daten sind Daten, die sich auf eine lebende Person beziehen, welche anhand dieser Daten (bzw. anhand dieser Daten in Kombination mit weiteren Informationen, die sich bereits in unserem Besitz befinden oder mit Wahrscheinlichkeit in unseren Besitz gelangen werden) identifizierbar ist.</p>
                            </li>
                            <li>
                                <p className="p"><strong>Nutzungsdaten</strong></p>
                                <p className="p">Nutzungsdaten sind Daten, die automatisch im Rahmen der Nutzung des Dienstes oder innerhalb der Dienstinfrastruktur selbst (beispielsweise für die Dauer eines Seitenbesuchs) erfasst werden.</p>
                            </li>
                            <li>
                                <p className="p"><strong>Cookies</strong></p>
                                <p className="p">Cookies sind kleine Dateien, die auf Ihrem Gerät (Computer oder mobiles Endgerät) gespeichert werden.</p>
                            </li>
                        </ul>

                        <h2 className="h2">Erfassung und Nutzung von Daten</h2>
                        <p className="p">Wir erfassen verschiedene Arten von Daten für eine Reihe von Zwecken, um den Dienst, den wir Ihnen zur Verfügung stellen, zu verbessern.</p>

                        <h3>Arten der erfassten Daten</h3>

                        <h4>Personenbezogene Daten</h4>
                        <p className="p">Im Rahmen der Nutzung unseres Dienstes bitten wir Sie gegebenenfalls um die Zurverfügungstellung bestimmter persönlich identifizierbarer Daten, die wir dazu nutzen, um Sie zu kontaktieren oder zu identifizieren ("personenbezogene Daten"). Persönlich identifizierbare Daten umfassen beispielsweise folgende Daten (sind jedoch nicht auf diese beschränkt):</p>

                        <ul className="ul">
                            <li>E-Mail-Adresse</li>    <li>Vorname und Nachname</li>        <li>Adresse, Staat, Provinz, Postleitzahl, Ort</li>    <li>Cookies und Nutzungsdaten</li>
                        </ul>

                        <h4>Nutzungsdaten</h4>

                        <p className="p">Wir können außerdem Daten zu der Art und Weise erfassen, auf welche auf unseren Dienst zugegriffen wird bzw. auf welche diese genutzt werden ("Nutzungsdaten"). Diese Nutzungsdaten umfassen gegebenenfalls die Internet-Protocol-Adresse (IP-Adresse) Ihres Computers, Ihren Browsertyp, Ihre Browserversion, die von Ihnen innerhalb unseres Dienstes besuchten Seiten, den Zeitpunkt und das Datum Ihres Besuchs, die Gesamtverweildauer auf den betreffenden Seiten, individuelle Geräteidentifikationsmerkmale und weitere Diagnostikdaten.</p>

                        <h4>Tracking & Cookies</h4>
                        <p className="p">Wir setzen Cookies und ähnliche Tracking-Technologien zur Überwachung der Aktivität innerhalb unseres Dienstes ein und speichern in diesem Zusammenhang bestimmte Daten.</p>
                        <p className="p">Cookies sind Dateien mit einem geringen Datenumfang, wie zum Beispiel anonyme einzigartige Identifikatoren. Cookies werden von einer Website an Ihren Browser gesendet und auf Ihrem Gerät gespeichert. Die sonstigen von uns eingesetzten Tracking-Technologien sind so genannte Beacons, Tags und Scripts und dienen der Erfassung und Nachverfolgung von Daten sowie der Verbesserung und Analyse unseres Dienstes.</p>
                        <p className="p">Sie können in den Einstellungen Ihres Browsers bestimmen, ob Sie alle Cookies ablehnen oder nur bestimmte Cookies akzeptieren möchten. Falls Sie jedoch die Annahme von Cookies verweigern, können Sie gegebenenfalls Teile unseres Dienstes nicht in Anspruch nehmen.</p>
                        <p className="p">Beispiele für von uns eingesetzte Cookies:</p>
                        <ul className="ul">
                            <li><strong>Sitzungs-Cookies.</strong> Wir setzen Sitzungs-Cookies für den Betrieb unseres Dienstes ein.</li>
                            <li><strong>Präferenz-Cookies.</strong> Wir setzen Präferenz-Cookies ein, um Ihre Präferenzen und verschiedenen Einstellungen zu speichern.</li>
                            <li><strong>Sicherheits-Cookies.</strong> Wir setzen Sicherheits-Cookies für Sicherheitszwecke ein.</li>
                        </ul>

                        <h2 className="h2">Datennutzung</h2>
                        <p className="p">Wir bei Collectif Mizo nutzen die erfassten Daten für verschiedene Zwecke, beispielsweise um:</p>
                        <ul className="ul">
                            <li>Ihnen unseren Dienst zur Verfügung zu stellen und diesen aufrecht zu erhalten;</li>
                            <li>Ihnen Änderungen in Bezug auf unseren Dienst mitzuteilen;</li>
                            <li>es Ihnen auf Wunsch zu ermöglichen, an den interaktiven Teilen unseres Dienstes teilzunehmen;</li>
                            <li>Kundendienstleistungen zur Verfügung zu stellen;</li>
                            <li>Analysedaten und sonstige wertvolle Daten zu erfassen, damit wir unseren Dienst verbessern können;</li>
                            <li>die Nutzung unseres Dienstes zu überwachen;</li>
                            <li>technische Probleme zu erkennen, zu vermeiden und zu beheben;</li>
                        </ul>

                        <h2 className="h2">Übertragung von Daten</h2>
                        <p className="p">Ihre Daten, einschließlich personenbezogener Daten, können auf Computer übertragen – und auf solchen aufbewahrt – werden, die sich außerhalb Ihres Heimatstaates, Ihrer Heimatprovinz, Ihres Heimatlandes oder einer sonstigen Rechtsordnung befinden und somit Datenschutzgesetzen unterliegen, die sich von den Datenschutzgesetzen in Ihrer Rechtsordnung unterscheiden.</p>
                        <p className="p">Falls Sie sich außerhalb von Belgium befinden und sich dazu entscheiden, Daten an uns zu übermitteln, müssen Sie zur Kenntnis nehmen, dass wir Ihre Daten, einschließlich personenbezogener Daten, nach Belgium übertragen und diese dort verarbeiten.</p>
                        <p className="p">Ihre Zustimmung zu dieser Datenschutz-Richtlinie und eine nachfolgende Übermittlung von Daten Ihrerseits stellt eine Einverständniserklärung Ihrerseits zu der genannten Übertragung dar.</p>
                        <p className="p">Collectif Mizo wird alle im zumutbaren Rahmen erforderlichen Schritte unternehmen um sicherzustellen, dass Ihre Daten auf sichere Weise sowie in Übereinstimmung mit dieser Datenschutz-Richtlinie behandelt werden, und dass Ihre personenbezogenen Daten nicht an Organisationen oder in Länder übertragen werden, hinsichtlich welcher keine hinreichenden Kontrollmechanismen in Bezug auf die Sicherheit Ihrer Daten und sonstigen personenbezogenen Informationen vorliegen.</p>

                        <h2 className="h2">Offenlegung von Daten</h2>

                        <h3>Gesetzliche Anforderungen</h3>
                        <p className="p">Collectif Mizo kann Ihre personenbezogenen Daten unter Umständen offenlegen, wenn es unter Beachtung der Grundsätze von Treu und Glauben der Ansicht ist, dass dies zur Erreichung der nachfolgenden Zielsetzungen erforderlich ist:</p>
                        <ul className="ul">
                            <li>zur Erfüllung einer gesetzlichen Pflicht</li>
                            <li>zum Schutz und zur Verteidigung der Rechte oder des Eigentums von Collectif Mizo</li>
                            <li>zur Vermeidung oder Untersuchung möglicher Fehlverhaltensweisen in Bezug auf den Dienst </li>
                            <li>zum Schutz der persönlichen Sicherheit der Nutzer des Dienstes oder der Öffentlichkeit</li>
                            <li>zur Vermeidung von Haftungsansprüchen</li>
                        </ul>

                        <h2 className="h2">Datensicherheit</h2>
                        <p className="p">Die Sicherheit Ihrer Daten ist uns wichtig. Bitte vergessen Sie jedoch nicht, dass es keine Übertragungsmethoden über das Internet und keine elektronischen Speichermedien gibt, die 100 % sicher sind. Obwohl wir stets bemüht sind, kommerziell annehmbare Maßnahmen zum Schutz Ihrer personenbezogenen Daten umzusetzen, können wir eine absolute Sicherheit nicht garantieren.</p>

                        <h2 className="h2">Leistungsanbieter</h2>
                        <p className="p">Wir beauftragen gegebenenfalls dritte Unternehmen und Einzelpersonen ("Leistungsanbieter") mit Unterstützungsleistungen zum einfacheren Angebot unseres Dienstes, mit der Erbringung von Leistungen in unserem Namen, mit der Erbringung von mit unserem Dienst verbundenen Leistungen oder mit Unterstützungsleistungen zur Analyse der Art und Weise, auf die unser Dienst in Anspruch genommen wird.</p>
                        <p className="p">Diese Dritten können auf Ihre personenbezogenen Daten nur in dem Umfang Zugriff nehmen, der für die Erfüllung der genannten Aufgaben in unserem Namen erforderlich ist, und dürfen diese für keine sonstigen Zwecke offenlegen oder nutzen.</p>

                        <h3>Analytik</h3>
                        <p className="p">Wir beauftragen gegebenenfalls dritte Leistungsanbieter mit der Überwachung und Analyse der Nutzung unseres Dienstes.</p>
                        <ul className="ul">
                            <li>
                                <p className="p"><strong>Google Analytics</strong></p>
                                <p className="p">Google Analytics ist ein von Google angebotener Web-Analytics-Dienst, der Zugriffe auf Websites nachverfolgt und meldet. Google nutzt die gewonnenen Daten zur Nachverfolgung und Überwachung der Nutzung unseres Dienstes. Diese Daten werden mit anderen Google-Diensten geteilt. Google kann die gewonnenen Daten zur Kontextualisierung und Personalisierung der Werbeanzeigen innerhalb seines eigenen Werbenetzwerks nutzen.</p>
                                <p className="p">Sie können die Übertragung Ihrer Aktivität innerhalb unseres Dienstes an Google Analytics abschalten, indem Sie das Browser-Add-on zur Deaktivierung von Google Analytics installieren. Das Add-on verhindert eine Datenübertragung an Google Analytics zu Besuchen bzw. Aktivität über das JavaScript von Google Analytics (ga.js, analytics.js und dc.js).</p>                <p className="p">Weitere Informationen zu den Datenschutzmaßnahmen von Google können Sie auf Googles Webseite zu seinen Datenschutzbestimmungen (Privacy Terms) einsehen: <a href="https://policies.google.com/privacy?hl=en">https://policies.google.com/privacy?hl=en</a></p>
                            </li>
                        </ul>


                        <h2 className="h2">Links zu anderen Websites</h2>
                        <p className="p">Unser Dienst kann Links zu anderen Websites enthalten, die nicht von uns betrieben werden. Wenn Sie auf einen Drittlink klicken, werden Sie direkt auf die Website des betreffenden Dritten weitergeleitet. Wir empfehlen Ihnen dringend, sich jeweils die Datenschutz-Richtlinien aller von Ihnen besuchten Websites durchzulesen.</p>
                        <p className="p">Wir haben keine Kontrolle über die Inhalte, Datenschutzvorschriften und -praktiken dritter Websites oder Dienste und übernehmen in diesem Zusammenhang keine Haftung.</p>


                        <h2 className="h2">Privatsphäre Minderjähriger</h2>
                        <p className="p">Unser Dienst richtet sich nicht an Personen, die das 18. Lebensjahr noch nicht vollendet haben ("minderjährige Personen").</p>
                        <p className="p">Wir erfassen wissentlich keine persönlich identifizierbaren Daten zu minderjährigen Personen. Falls Sie ein Elternteil oder Vormund sind und es Ihnen bekannt wird, dass eine Ihrer Aufsicht unterstehende minderjährige Person uns personenbezogene Daten übermittelt hat, bitten wir Sie, mit uns Kontakt aufzunehmen. Falls uns bekannt wird, dass wir personenbezogene Daten einer minderjährigen Person ohne elterliche Zustimmung erfasst haben, setzen wir Maßnahmen zur Entfernung dieser Daten von unseren Servern um.</p>


                        <h2 className="h2">Änderungen dieser Datenschutz-Richtlinie</h2>
                        <p className="p">Wir können unsere Datenschutz-Richtlinie von Zeit zu Zeit aktualisieren. Jegliche solcher Änderungen teilen wir Ihnen mit, indem wir die aktualisierte Fassung auf dieser Seite veröffentlichen.</p>
                        <p className="p">Wir werden Sie vor dem Inkrafttreten der betreffenden Änderung per E-Mail und/oder mittels einer sonstigen sichtbaren Mitteilung innerhalb unseres Dienstes informieren und das "Datum des Inkrafttretens" am Beginn dieser Datenschutz-Richtlinie aktualisieren.</p>
                        <p className="p">Wir empfehlen Ihnen, diese Datenschutz-Richtlinie regelmäßig auf Änderungen hin durchzusehen. Änderungen dieser Datenschutz-Richtlinie werden im Zeitpunkt ihrer Veröffentlichung auf dieser Seite wirksam.</p>


                        <h2 className="h2">Kontaktaufnahme</h2>
                        <p className="p">Falls Sie Fragen zu dieser Datenschutz-Richtlinie haben, können Sie wie folgt Kontakt zu uns aufnehmen:</p>
                        <ul className="ul">
                            <li>Per E-Mail: info@unrhythmic.be</li>
                            <li>Durch Besuch der folgenden Seite unserer Website: https://www.unrhythmic.be/privacy-policy</li>

                        </ul>
                    </div>
                    <div className="privacy-policy">
                        <h1>Privacybeleid</h1>


                        <p className="p">Ingangsdatum: April 16, 2019</p>


                        <p className="p">Collectif Mizo ("ons", "wij" of "onze") beheert de https://www.unrhythmic.be website ("hierna genoemd Dienst").</p>

                        <p className="p">Deze pagina bevat informatie over ons beleid met betrekking tot de verzameling, het gebruik en de openbaarmaking van uw persoonsgegevens wanneer u onze Dienst gebruikt en de keuzes die u hebt met betrekking tot die gegevens. Our Privacy Policy  for Collectif Mizo is created with the help of the <a href="https://www.freeprivacypolicy.com/free-privacy-policy-generator.php">Free Privacy Policy Generator</a>.</p>

                        <p className="p">Wij gebruiken uw gegevens om de Dienst te leveren en te verbeteren. Wanneer u de Dienst gebruikt, gaat u akkoord met de verzameling en het gebruik van informatie in overeenstemming met dit beleid. Tenzij anders bepaald in dit Privacybeleid heeft de terminologie die wordt gebruikt in dit Privacybeleid dezelfde betekenis als in onze Algemene voorwaarden, beschikbaar op https://www.unrhythmic.be</p>

                        <h2 className="h2">Definities</h2>
                        <ul className="ul">
                            <li>
                                <p className="p"><strong>Dienst</strong></p>
                                <p className="p">Onder dienst verstaan wij de https://www.unrhythmic.be website beheerd door Collectif Mizo</p>
                            </li>
                            <li>
                                <p className="p"><strong>Gebruiksgegevens</strong></p>
                                <p className="p">Onder gebruiksgegevens verstaan wij automatisch verzamelde gegevens die worden gegenereerd door het gebruik van de Dienst of van de infrastructuur van de Dienst zelf (bijvoorbeeld, de duur van het bezoek aan een pagina).</p>
                            </li>
                            <li>
                                <p className="p"><strong>Gebruiksgegevens</strong></p>
                                <p className="p">Onder gebruiksgegevens verstaan wij automatisch verzamelde gegevens die worden gegenereerd door het gebruik van de Dienst of van de infrastructuur van de Dienst zelf (bijvoorbeeld, de duur van het bezoek aan een pagina).</p>
                            </li>
                            <li>
                                <p className="p"><strong>Cookies</strong></p>
                                <p className="p">Cookies zijn informatiebestandjes die worden opgeslagen op uw apparaat (computer of mobiele apparaat).</p>
                            </li>
                        </ul>

                        <h2 className="h2">Gegevensverzameling en gebruik</h2>
                        <p className="p">Wij verzamelen verschillende soorten gegevens voor uiteenlopende doeleinden om onze Dienst aan u te kunnen leveren en om hem te verbeteren.</p>

                        <h3>Soorten gegevens die worden verzameld</h3>

                        <h4>Persoonsgegevens</h4>
                        <p className="p">Wanneer u onze Dienst gebruikt, kunnen wij u vragen ons bepaalde persoonlijk identificeerbare informatie te verstrekken die kan worden gebruikt om contact op te nemen met u of om u te identificeren ("Persoonsgegevens"). Deze persoonlijk identificeerbare informatie kan omvatten maar is niet beperkt tot:</p>

                        <ul className="ul">
                            <li>E-mailadres</li>    <li>Voor- en achternaam</li>        <li>Adres, provincie, postcode, stad</li>    <li>Cookies en Gebruiksgegevens</li>
                        </ul>

                        <h4>Gebruiksgegevens</h4>

                        <p className="p">Wij kunnen ook gegevens verzamelen over de wijze waarop de gebruiker toegang krijgt tot de Dienst en hoe deze wordt gebruikt ("Gebruiksgegevens") Deze Gebruiksgegevens kunnen informatie bevatten zoals het Internet Protocol adres (IP-adres) van uw computer, het type browser, de versie van de browser, de pagina's die u hebt bezocht op onze Dienst, het tijdstip en de datum van uw bezoek, de tijd die u hebt doorgebracht op die pagina's, unieke apparaat-ID's en andere diagnostische gegevens.</p>

                        <h4>Tracking & cookiegegevens</h4>
                        <p className="p">Wij gebruiken cookies en soortgelijke volgtechnologieën om de activiteit op onze Dienst te volgen en we bewaren bepaalde informatie.</p>
                        <p className="p">Cookies zijn bestanden met een kleine hoeveelheid gegevens die een anonieme unieke ID kunnen bevatten. Cookies worden van een website verzonden naar uw browser en opgeslagen op uw apparaat. Er worden ook andere volgtechnologieën gebruikt zoals beacons, tags en scripts om gegevens te verzamelen en te volgen en om onze Dienst te verbeteren en te analyseren.</p>
                        <p className="p">U kunt uw browser instellen om alle cookies te weigeren of om aan te geven wanneer een cookie wordt verzonden. Maar als u geen cookies aanvaardt, kunt u mogelijk niet alle functies van onze Dienst gebruiken.</p>
                        <p className="p">Voorbeelden van cookies die wij gebruiken:</p>
                        <ul className="ul">
                            <li><strong>Sessiecookies.</strong> Wij gebruiken Sessiecookies om onze Dienst te beheren.</li>
                            <li><strong>Voorkeurcookies.</strong>  Wij gebruiken Voorkeurcookies om uw voorkeuren en uiteenlopende instellingen bij te houden.</li>
                            <li><strong>Veiligheidscookies.</strong> Wij gebruiken Veiligheidscookies voor veiligheidsdoeleinden.</li>
                        </ul>

                        <h2 className="h2">Gebruik van gegevens</h2>
                        <p className="p">Collectif Mizo gebruikt de verzamelde gegevens voor uiteenlopende doeleinden:</p>
                        <ul className="ul">
                            <li>Om onze Dienst te leveren en te onderhouden</li>
                            <li>Om u wijzigingen in onze Dienst te melden</li>
                            <li>Om u de mogelijkheid te bieden om, indien gewenst, deel te nemen aan de interactieve functies van onze Dienst</li>
                            <li>Om onze klanten steun te verlenen</li>
                            <li>Om analyse- of waardevolle gegevens te verzamelen die we kunnen toepassen om onze Dienst te verbeteren</li>
                            <li>Om toezicht te houden op het gebruik van onze Dienst</li>
                            <li>Om technische problemen te detecteren, te voorkomen en te behandelen</li>
                            <li>Om u nieuws, speciale aanbiedingen en algemene informatie te bieden over onze goederen, diensten en evenementen die gelijkaardig zijn aan wat u in het verleden al gekocht hebt of waar u informatie over hebt gevraagd, tenzij u hebt aangegeven dat u dergelijke informatie niet wenst te ontvangen.</li>
                        </ul>

                        <h2 className="h2">Overdracht van gegevens</h2>
                        <p className="p">Uw gegevens, inclusief Persoonsgegevens, kunnen worden overgedragen naar — en bewaard op — computers die zich buiten het rechtsgebied van uw provincie, land of een andere overheidsinstantie bevinden waar de wetgeving inzake gegevensbescherming kan verschillen van de wetgeving in uw rechtsgebied.</p>
                        <p className="p">Let erop dat, als u zich buiten Belgium bevindt en u ons gegevens verstrekt, wij deze gegevens, inclusief Persoonsgegevens, overdragen naar Belgium en ze daar verwerken.</p>
                        <p className="p">Uw instemming met dit Privacybeleid gevolgd door uw indiening van dergelijke gegevens geven aan dat u akkoord gaat met die overdracht.</p>
                        <p className="p">Collectif Mizo zal alle redelijkerwijs noodzakelijke stappen ondernemen om ervoor te zorgen dat uw gegevens veilig en in overeenstemming met dit Privacybeleid worden behandeld en dat uw Persoonsgegevens nooit worden overgedragen aan een organisatie of een land als er geen gepaste controles zijn ingesteld, inclusief de veiligheid van uw gegevens en andere persoonsgegevens.</p>

                        <h2 className="h2">Openbaarmaking van gegevens</h2>

                        <h3>Wettelijke vereisten</h3>
                        <p className="p">Collectif Mizo kan uw Persoonsgegevens openbaar maken als het te goeder trouw de mening is toegedaan dat een dergelijke handeling noodzakelijk is:</p>
                        <ul className="ul">
                            <li>Om te voldoen aan een wettelijke verplichting</li>
                            <li>Om de rechten en eigendom van Collectif Mizo te beschermen en te verdedigen</li>
                            <li>Om mogelijke misstanden te voorkomen of te onderzoeken in verband met de Dienst</li>
                            <li>Om de persoonlijke veiligheid van gebruikers van de Dienst of het publiek te beschermen</li>
                            <li>Als bescherming tegen juridische aansprakelijkheid</li>
                        </ul>

                        <h2 className="h2">Veiligheid van gegevens</h2>
                        <p className="p">De veiligheid van uw gegevens is belangrijk voor ons, maar vergeet niet dat geen enkele methode van verzending via het internet of elektronische methode van opslag 100% veilig is. Hoewel wij ernaar streven commercieel aanvaardbare middelen toe te passen om uw Persoonsgegevens te beschermen, kunnen wij de absolute veiligheid niet waarborgen.</p>

                        <h2 className="h2">Dienstverleners</h2>
                        <p className="p">Wij kunnen externe bedrijven en personen aanstellen om onze Dienst ("Dienstverleners") te vereenvoudigen, om de Dienst te leveren in onze naam, om diensten uit te voeren in het kader van onze Dienst of om ons te helpen bij de analyse van hoe onze Dienst wordt gebruikt.</p>
                        <p className="p">Deze externe partijen hebben enkel toegang tot uw Persoonsgegevens om deze taken uit te voeren in onze naam en zij mogen deze niet openbaar maken aan anderen of ze gebruiken voor andere doeleinden.</p>

                        <h3>Analytics</h3>
                        <p className="p">Wij kunnen beroep doen op externe Dienstverleners om het gebruik van onze Dienst te volgen en te analyseren.</p>
                        <ul className="ul">
                            <li>
                                <p className="p"><strong>Google Analytics</strong></p>
                                <p className="p">Google Analytics is een webanalyse-service van Google die het websiteverkeer volgt en rapporteert. Google gebruikt de verzamelde gegevens om het gebruik van onze Dienst te volgen en bij te houden. Deze gegevens worden gedeeld met andere Google diensten. Google kan de verzamelde gegevens gebruiken om de advertenties van zijn eigen advertentienetwerk te contextualiseren en te personaliseren.</p>
                                <p className="p">U kunt aangeven dat u uw activiteit op de Dienst niet beschikbaar wenst te maken voor Google Analytics door de Google Analytics opt-out browser add-on te installeren. Deze add-on zorgt ervoor dat het Google Analytics JavaScript (ga.js, analytics.js en dc.js) geen informatie kan delen met Google Analytics over uw activiteiten op het internet.</p>                <p className="p">Voor meer informatie over de privacypraktijken van Google verwijzen wij u naar de internetpagina van Google Privacy en voorwaarden: <a href="https://policies.google.com/privacy?hl=en">https://policies.google.com/privacy?hl=en</a></p>
                            </li>
                        </ul>


                        <h2 className="h2">Links naar andere sites</h2>
                        <p className="p">Onze Dienst kan links bevatten naar andere sites die niet door ons worden beheerd. Als u klikt op een link van een externe partij wordt u naar de site van die externe partij gebracht. Wij raden u sterk aan het Privacybeleid te verifiëren van elke site die u bezoekt.</p>
                        <p className="p">Wij hebben geen controle over en aanvaarden geen aansprakelijkheid met betrekking tot de inhoud, het privacybeleid of de privacypraktijken van de sites of diensten van een externe partij.</p>


                        <h2 className="h2">Privacy van kinderen</h2>
                        <p className="p">Onze dienst richt zich niet op personen die jonger zijn dan 18 ("Kinderen").</p>
                        <p className="p">Wij verzamelen nooit bewust persoonlijk identificeerbare informatie van iemand die jonger is dan 18 jaar oud. Als u een ouder of voogd bent en u stelt vast dat uw kind ons persoonsgegevens heeft geleverd, vragen wij u contact op te nemen met ons. Als u vaststelt dat wij persoonsgegevens hebben verzameld van kinderen zonder de verificatie van ouderlijk toezicht zullen wij de nodige stappen ondernemen om die informatie te verwijderen van onze servers.</p>


                        <h2 className="h2">Wijzigingen aan dit Privacybeleid</h2>
                        <p className="p">Wij kunnen ons Privacybeleid op gezette tijden bijwerken. Wij zullen u op de hoogte brengen van eventuele wijzigingen door het nieuwe Privacybeleid te publiceren op deze pagina.</p>
                        <p className="p">Wij zullen u op de hoogte brengen via e-mail en/of een duidelijke melding op onze Dienst voor de wijzigingen van kracht gaan en wij zullen de "aanvangsdatum" bijwerken die vermeld staat bovenaan in dit Privacybeleid.</p>
                        <p className="p">Wij raden u aan dit Privacybeleid regelmatig te controleren op eventuele wijzigingen. Wijzigingen aan dit Privacybeleid gaan van kracht op het moment dat ze worden gepubliceerd op deze pagina.</p>


                        <h2 className="h2">Contact opnemen</h2>
                        <p className="p">Als u vragen hebt over dit Privacybeleid kunt u contact opnemen met ons:</p>
                        <ul className="ul">
                            <li>Via emails: info@unrhythmic.be</li>
                            <li>Via deze pagina op onze website: https://www.unrhythmic.be/privacy-policy</li>
                        </ul>
                    </div>
                </section>*/
