import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Public from "../components/Page/Public";
import Post from "../components/Block/Post";

export default class Blog extends Component {

    constructor(props) {
        super(props);
    };

    render() {
        return (
            <Public title="LAST NEWS">
                <section id="content-top">
                    <div id="content-top__div">
                        {
                            // show first post (sorted by createdAt in HomeContainer.jsx)
                            this.props.posts.length > 0 ?
                            this.props.posts.map((post, i) => (
                                i === 0 ?
                                    <Post key={i + 1} i={i + 1} post={post} showAll={ false }/>
                                    : ''
                            )) : (<section className="content-background-top"><h2>No news published</h2>
                                </section>)
                        }
                    </div>
                </section>
                <section id="content-bottom">
                    <div id="content-bottom__div">
                        {
                            // show x posts except the first one (x = limit mongo directive in query in HomeContainer.jsx)
                            this.props.posts.map((post,i) => (
                                i !== 0 ?
                                    <Post key={i+1} i={i+1} post={post} showAll={ false }/>
                                    : ''
                            ))
                        }
                    </div>
                </section>
            </Public>
        );
    };
}

Blog.propTypes = {
    // current meteor user
    user: PropTypes.object
};
