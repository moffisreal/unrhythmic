import React from "react";
import ReactDOM from 'react-dom';
import {
    Redirect,
} from "react-router-dom";

import Auth from "../../components/Page/Auth.jsx";
import Form from "../../components/Form/Form";
import Button from "../../components/Form/Button";
import Error from "../../components/Form/Error";

import './style/SignUpForm.less';

// TODO import Loader from "../../components/Loader/Loader"; // MIGHT CHANGE TO const Menu = () => {
// LOOK AT https://reactjs.org/docs/react-component.html#unsafe_componentwillreceiveprops > use componentDidUpdate for animation while prop change e.g.

/**
 * @description Represents a sign up form
 * @extends Form
 */
export default class SignUpForm extends Form {

    constructor (props) {
        super (props);

        // event binding
        this.submitSignUpForm.bind(this);

        // form fields name
        this.username = 'username';
        this.email = 'email';
        this.password = 'password';
        this.passwordRepeat = 'passwordRepeat';

        // form elements definition
        this.elements = {
            username: {
                name: this.username,
                type: 'text',
                text: "Your user name",
                error: null
            },
            email: {
                name: this.email,
                type: 'text',
                text: "Your emails",
                error: null
            },
            password: {
                name: this.password,
                type: 'password',
                text: "Password: min 8 characters with 1 uppercase letter, 1 digit and one special character (-,/,*,=,etc)",
                error: null
            },
            passwordRepeat: {
                name: this.passwordRepeat,
                type: 'password',
                text: "Retype your password",
                error: null
            },
        };
    };

    /**
     * @description Management of the sign up form's submit event
     * @param event
     */
    submitSignUpForm = (event ) => {
        event.preventDefault();

        const username = ReactDOM.findDOMNode( this.refs.username.refs.username ).value.trim();
        const password = ReactDOM.findDOMNode( this.refs.password.refs.password ).value;
        const email = ReactDOM.findDOMNode(this.refs.email.refs.email).value.trim();

        // defined in meteor doc as default user object profile value
        const userProfile = {
            username: username,
            email: email,
            password: password,
            passwordRepeat: ReactDOM.findDOMNode(this.refs.passwordRepeat.refs.passwordRepeat).value,
            profile: {
                'name': {
                    'first': username, 'last': username
                }
            }
        };

        // sanitize form values
        Form.sanitizeValues(userProfile);

        // call method insert from users methods
        Meteor.call("users.insert", userProfile, (errors) => {
            if (errors) {
                this.setState({
                    error : errors.error === 403 ? errors.reason : null,
                    formErrors: errors
                });
            }
            else {
                this.resetErrors();
                Meteor.loginWithPassword(username, password, (error) => {
                    if (error) {
                        Form.error = error.reason;
                    }
                })
            }
        });
    };

    /**
     * @description Renders the Sign Up form
     * @returns {*}
     */
    render() {
        this.elements = Form.setFormElementsErrors( this.elements, this.state.formErrors );

        return (
            <Auth title="Register">
                { Meteor.user() ?
                    <Redirect
                        to={{
                            pathname: "/"
                        }}
                    />
                :
                    <section id="content-top">
                        <div id="content-top__div">
                            <div className="content-background-top">
                                <Error error={ Form.error === null ? this.state.error : Form.error }/>
                                <form onSubmit={ this.submitSignUpForm }>
                                    {
                                        // loop on form elements and render them trough Input Component
                                        Form.renderElements(this.elements)
                                    }
                                    <Button name="submit" type="submit" text="Register"/>
                                </form>
                            </div>
                        </div>
                    </section>
                }
            </Auth>
        );
    }
}
