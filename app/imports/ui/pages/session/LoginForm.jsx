import React from "react";
import { Link, Redirect } from "react-router-dom";
import ReactDOM from "react-dom";

import Auth from "../../components/Page/Auth.jsx";
import Form from "../../components/Form/Form";
import Error from "../../components/Form/Error";
import Button from "../../components/Form/Button";
import FacebookAuthButton from "../../components/Block/FacebookAuthButton";

import './style/LoginForm.less';

export default class LoginForm extends Form {

    constructor(props){
        super(props);
        // form fields name
        this.username = 'username';
        this.password = 'password';

        // form elements definition
        this.elements = {
            username: {
                name: this.username,
                type: 'text',
                text: "Your user name",
                error: null
            },
            password: {
                name: this.password,
                type: 'password',
                text: "Your password",
                error: null
            },
        };

        this.submitLoginForm.bind(this);
    };

    submitLoginForm = (e) => {

        e.preventDefault();

        // get form values
        const username = ReactDOM.findDOMNode(this.refs.username.refs.username).value.trim();
        const password = ReactDOM.findDOMNode(this.refs.password.refs.password).value;

        const userLogin = {
            username: username,
            password: password
        };

        // sanitize form values
        Form.sanitizeValues(userLogin);

        // login
        Meteor.loginWithPassword( userLogin.username, userLogin.password,  ( error ) => {
            if ( error ) {
                let errorMessage = "";
                switch (error.reason) {
                    // if user or password is undefined.
                    case "Unrecognized options for login request":
                        errorMessage = "Unrecognized options for login";
                        break;
                    // if user isn’t an Object or String, or password isn’t a String.
                    case "Match failed":
                    case "User not found ":
                    case "Incorrect password":
                        errorMessage = "User name or password incorrect";
                        break;
                    // if user doesn’t have a password.
                    case "User has no password set":
                        errorMessage = "User does not have a password yet";
                        break;
                    default:
                        errorMessage = "Unknown error ..";
                        break;
                }
                Form.error = errorMessage;
            }
        });
    };

    render() {

        const extras = '';

        return (
            <Auth title="LOGIN" extras={ Meteor.user() ? '' : extras }>
                { Meteor.user() ?
                    <Redirect to={{ pathname: "/" }}/>
                    :
                    <section id="content-top">
                        <div id="content-top__div">
                            <div id="login-form" className="content-background-top">
                                <FacebookAuthButton/>
                                <p className="or"> OR </p>
                                <Error type="top" error={ Form.error }/>
                                <form onSubmit={ this.submitLoginForm }>
                                    {
                                        // loop on form elements and render them trough Input Component
                                        Form.renderElements(this.elements)
                                    }
                                    <Button name="submit" type="submit" text="Connexion"/>
                                </form>
                                <p className="or"> OR </p>
                                <Link to="/sign-up">Sign up</Link>
                            </div>
                        </div>
                    </section>
                }
            </Auth>
        );
    };
}
