import React, {Component} from "react";
import Button from "../../components/Form/Button";

import './style/NoMatch.less';
import Public from "../../components/Page/Public";

export default class NoMatch extends Component{

    constructor(props) {
        super();
        this.history = props.history;
        this.location = props.location;
    };

    render() {
        return <Public title="NOT FOUND">
                <section id="content-top">
                    <div id="content-top__div">
                        <article className="content-background-top">
                            <div id="not-found">
                                <h2 className="h2">404 - Mmmh, it looks like <code>{ this.location.pathname }</code> <br/>does not exist anymore :(</h2>
                                <img src="/img/404.svg" width="250" height="250"/>
                            </div>
                            <div>
                                <Button id="notFound__button" name="back" type="text" text="Go Back Here" onClick={ this.history.goBack } />
                            </div>
                        </article>
                    </div>
                </section>
            </Public>
    }
};
