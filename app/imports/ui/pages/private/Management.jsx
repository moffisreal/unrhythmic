import React, { Component } from "react/react";

import Admin from "../../components/Page/Admin.jsx";
import { Link } from "react-router-dom";

import "./posts/style/Management.less";

export default class Management extends Component {

    render() {
        return (
            <Admin title="ADMIN">
                <section id="content-top">
                    <div id="content-top__div">
                        <section className="content-background-top">
                            <h2 className="h2">Management</h2>
                            <ul id="admin-list">
                                <li>
                                    <p>Create a news :</p>
                                    <Link to="/management/create-post">Create news</Link>
                                </li>
                                <li>
                                    <p>Manage all news:</p>
                                    <Link to="/management/post-list">Show all news</Link>
                                </li>
                                <li>
                                    <p>Add an admin (coming soon):</p>
                                    <Link to="/management/users-list">Show all users</Link>
                                </li>
                            </ul>
                        </section>
                    </div>
                </section>
            </Admin>
        );
    }
}

