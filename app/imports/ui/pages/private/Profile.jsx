import React, { Component } from "react/react";

import Private from "../../components/Page/Private";
import PasswordUpdateForm from "./profile/PasswordUpdateForm";

import './style/Profile.less';

export default class Profile extends Component {

    render() {
        return (
            <Private title="Your Account">
                <section id="content-top">
                    <div id="content-top__div">
                        <article className="content-background-top">
                            <div className="hello">Hello
                                <span> { this.props.user ? this.props.user.username : "" }</span>
                            </div>
                            <h3 className="h4">Password Management</h3>
                            <PasswordUpdateForm/>
                        </article>
                    </div>
                </section>
            </Private>
        );
    }
}
