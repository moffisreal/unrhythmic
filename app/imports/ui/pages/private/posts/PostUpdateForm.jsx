import React from "react";
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';

import { Link } from "react-router-dom";

import Form from "../../../components/Form/Form";
import Error from "../../../components/Form/Error";
import Success from "../../../components/Form/Success";
import Button from "../../../components/Form/Button";

import './style/Management.less';
import Admin from "../../../components/Page/Admin";

export default class PostUpdateForm extends Form {

    content = "";

    modules = {
        toolbar: [
            [{ 'header': [2, 3, 4, 5, false] }],
            [{ 'font': [] }],
            ['underline', 'blockquote'],
            [{'list': 'ordered'}, {'list': 'bullet'}, {'indent': '-1'}, {'indent': '+1'}],
            ['link', 'image'],
            ['clean']
        ],
    };

    formats = [
        'header',
        'bold', 'italic', 'underline', 'strike', 'blockquote',
        'list', 'bullet', 'indent',
        'link', 'image'
    ];

    constructor( props ){

        super ( props );

        this.state = {
            text: null,
            delta: null,
            firstView: true,
            error:  null,
            success: null,
            firstChange: true
        };

        // events
        this.handleChange = this.handleChange.bind(this);
        this.submitPostUpdateForm = this.submitPostUpdateForm.bind(this);
    };

    submitPostUpdateForm = (e) => {

        e.preventDefault();

        let content = Form.removeScriptTag(this.state.content);
        content = Form.encodeHtmlEntities(content);

        Meteor.call("posts.update", {
            id: this.props.post[0]._id,
            text: content,
            delta: this.state.delta
        }, (errors) => {
            if (errors) {
                this.setState({
                    error: errors.details[0].type
                });
            } else {
                this.setState({
                    error: null,
                    success: "News successfully modified"
                });
            }
        });
    };

    componentDidMount(){
        this.setState({
            text: JSON.parse(this.props.post[0].delta),
            firstChange: false
        });
    }

    handleChange(value) {
        if (!this.state.firstChange){
            let quillRef = this.refs.editor;
            this.setState({
                text: value,
                content: quillRef.getEditorContents(),
                delta: JSON.stringify(quillRef.getEditor().getContents())
            });
        }
    };

    render() {
        return (
            <Admin title="News Update">
                <section id="content-top">
                    <div id="content-top__div">
                        <div className="content-top-list">
                            {
                                <article className="article">
                                    <Success type="top" success={ this.state.success }/>
                                    <Error type="top" error={ this.state.error  }/>
                                    {
                                        this.state.success === null ?
                                            (
                                                <div>
                                                    <ReactQuill name="editor" theme="snow" value={
                                                        this.state.text
                                                    }
                                                                onChange={ this.handleChange }
                                                                modules={ this.modules }
                                                                formats={ this.formats }
                                                                ref="editor"
                                                    />
                                                    <Button name="submit" type="submit" text="Modifier" onClick={ this.submitPostUpdateForm }/>
                                                </div>
                                            ):
                                            (
                                                <div>
                                                    <Link className="back__home" to={"/"}>Accueil</Link>
                                                    <Link className="back__home" to={"/management/post-list"}>Liste des posts</Link>

                                                </div>
                                            )
                                    }
                                </article>
                            }
                        </div>
                    </div>
                </section>
                <section id="content-bottom">
                    <div id="content-bottom__div">
                        <div className="content-bottom__article--even">

                        </div>
                    </div>
                </section>
            </Admin>
        );
    }

}
