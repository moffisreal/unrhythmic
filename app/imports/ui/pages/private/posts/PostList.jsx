import React, {Component} from "react";

import Admin from "../../../components/Page/Admin";
import Post from "../../../components/Block/Post";

export default class PostList extends Component {
    constructor(props){
        super(props);
    }
    render(){
       return <Admin title="News List">
           <section id="content-top">
               <div id="content-top__div">
                   <div className="content-top-list">
                   {
                       // show first post (sorted by createdAt in PostListContainer.jsx)
                       this.props.posts.map((post, i) => (
                           <Post key={i+1} i={i+1} post={post} showAll={ false } type="list"/>
                       ))
                   }
                   </div>
               </div>
           </section>
           <section id="content-bottom">
               <div id="content-bottom__div">
               </div>
           </section>
        </Admin>
    }
}
