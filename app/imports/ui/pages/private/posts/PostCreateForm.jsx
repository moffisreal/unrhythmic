import React from "react";
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';

import Form from "../../../components/Form/Form";
import Error from "../../../components/Form/Error";
import Button from "../../../components/Form/Button";

import './style/Management.less';
import Admin from "../../../components/Page/Admin";
import Success from "../../../components/Form/Success";

import {
    Link,
} from "react-router-dom";

export default class PostCreateForm extends Form {

    modules = {
        toolbar: [
            [{ 'header': [2, 3, 4, 5, false] }],
            [{ 'font': [] }],
            ['underline', 'blockquote'],
            [{'list': 'ordered'}, {'list': 'bullet'}, {'indent': '-1'}, {'indent': '+1'}],
            ['link', 'image'],
            ['clean']
        ],
    };

    formats = [
        'header',
        'bold', 'italic', 'underline', 'strike', 'blockquote',
        'list', 'bullet', 'indent',
        'link', 'image'
    ];

    constructor(props){

        super(props);

        this.state = {
            text: "",
            content: "",
            delta: "",
            error: null,
            success: null
        };

        this.handleChange = this.handleChange.bind(this);
        this.submitPostCreateForm.bind(this);
    };

    submitPostCreateForm = (e) => {

        e.preventDefault();

        let content = Form.removeScriptTag(this.state.content);
        content = Form.encodeHtmlEntities(content);

        Meteor.call("posts.insert", {
            text: content,
            delta: this.state.delta
        }, (errors) => {
            if (errors) {
                console.log(errors);
                this.setState({
                    error: errors
                });
            } else {
                this.setState({
                    error: null,
                    success: "News created and displayed in \'home\'"
                });
            }
        });
    };

    handleChange(value) {
        let quillRef = this.refs.editor;
        // when nothin in quill editor, remove surrounding <p> tags.
        // or the content will not be empty and error not displayed correctly.
        if (quillRef.state.selection !== null && quillRef.state.selection.index === 0){
            this.setState({
                content: "",
                text: "",
                delta: ""
            })
        } else {
            this.setState({
                text: value,
                content: quillRef.getEditorContents(),
                delta: JSON.stringify(quillRef.getEditor().getContents())
            });
        }
    };

    render() {
        return (
            <Admin title="Create News">
                <section id="content-top">
                    <div id="content-top__div">
                        <div className="content-top-list scrollable">
                            <Success type="top" success={ this.state.success }/>
                            <Error type="top" error={ this.state.error  }/>
                            {
                                this.state.success === null ?
                                    (
                                        <div>
                                            <ReactQuill name="editor" theme="snow" value={ this.state.text }
                                                        onChange={ this.handleChange }
                                                        modules={ this.modules }
                                                        formats={ this.formats }
                                                        ref="editor"
                                            />
                                            <Button name="submit" type="submit" text="submit" onClick={ this.submitPostCreateForm }/>
                                        </div>
                                    ):
                                    (
                                        <Link className="back__home" to={"/"}>Back To Home</Link>
                                    )
                            }
                        </div>
                    </div>
                </section>
                <section id="content-bottom">
                    <div id="content-bottom__div">
                        <div className="content-bottom__article--even">
                        </div>
                    </div>
                </section>
            </Admin>
        );
    }
}
