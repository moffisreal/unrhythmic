import React, {Component} from "react";

import Admin from "../../../components/Page/Admin";
import UserItem from "../../../components/Block/UserItem";

export default class UsersList extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return <Admin title="List news">
            <section id="content-top">
                <div id="content-top__div">
                    <div className="content-top-list">
                        {
                            this.props.users.map((user, i) => (
                                <UserItem key={i+1} i={i+1} user={user} type="list"/>
                            ))
                        }
                    </div>
                </div>
            </section>
            <section id="content-bottom">
                <div id="content-bottom__div">
                </div>
            </section>
        </Admin>
    }
}
