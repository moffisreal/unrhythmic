import React from "react";

import Form from "../../../components/Form/Form";
import Error from "../../../components/Form/Error";
import Success from "../../../components/Form/Success";
import Button from "../../../components/Form/Button";
import {
    Link
} from "react-router-dom";
import {Accounts} from "meteor/accounts-base";
import ReactDOM from "react-dom";

export default class PasswordUpdateForm extends Form {

    constructor( props ) {
        super ( props );

        // form elements definition
        this.elements = {
            oldPassword: {
                name: 'oldPassword',
                type: 'password',
                text: "Type your old password",
                error: null
            },
            newPassword: {
                name: 'newPassword',
                type: 'password',
                text: "New password, min 8 characters with 1 uppercase letter, 1 digit and one special character (-,/,*,=,etc)",
                error: null
            },
            passwordRepeat: {
                name: 'passwordRepeat',
                type: 'password',
                text: "Retype your new password",
                error: null
            }
        };

        this.state = {
            success: null,
            error: null
        };

        this.submitPasswordUpdateForm.bind(this);
    };

    submitPasswordUpdateForm = (e) => {

        e.preventDefault();

        const passwords = {
            newPassword: ReactDOM.findDOMNode(this.refs.newPassword.refs.newPassword).value,
            passwordRepeat: ReactDOM.findDOMNode(this.refs.passwordRepeat.refs.passwordRepeat).value
        };

        Meteor.call("users.resetPassword", passwords, (errors) => {
            if (errors) {
                this.setState({
                    error: errors.details[0].type
                });
            }
            else{
                Accounts.changePassword(
                    ReactDOM.findDOMNode(this.refs.oldPassword.refs.oldPassword).value,
                    ReactDOM.findDOMNode(this.refs.newPassword.refs.newPassword).value,
                    (error) => {
                        if (error) {
                            this.setState({
                                error: "Current password invalid, please retry"
                            })
                        }
                        else{
                            this.setState({
                                error: null,
                                success: "Password successfully modified bro\'"
                            });
                        }
                    }
                );
            }
        });
    };

    render() {
        return (
                <article className="article">
                    <Success type="top" success={this.state.success}/>
                    <Error type="top" error={this.state.error}/>
                    {
                        this.state.success === null ?
                            (
                                <div>
                                    <form onSubmit={ this.submitPasswordUpdateForm }>
                                        {
                                            // loop on form elements and render them trough Input Component
                                            Form.renderElements(this.elements)
                                        }
                                        <Button name="submit" type="submit" text="Modifier Mot de passe"/>
                                    </form>
                                </div>
                            ) :
                            (
                                <div>
                                    <Link className="back__home" to={"/"}>Home</Link>
                                </div>
                            )
                    }
                </article>
        )
    }
}
