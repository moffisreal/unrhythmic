import React, { Component } from 'react';
import Public from "../components/Page/Public";
import "./style/Home.less";

export default class Home extends Component {

    constructor(props) {
        super(props);
    };
    render() {
        return (
            <Public>
                <section id="content-top--no-title">

                </section>

                <section id="content-bottom">
                    <div id="content-bottom__div">
                        <div className="content-bottom__article--even">
                        </div>
                    </div>
                </section>
            </Public>
        );
    };
}
