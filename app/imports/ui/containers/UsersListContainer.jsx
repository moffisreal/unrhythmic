import React from "react";
import { withTracker } from "meteor/react-meteor-data";
import UsersList from "../pages/private/users/UsersList";
import { Users } from "../../api/users/users";

const UsersListContainer = withTracker(() => {
    Meteor.subscribe("users.list");
    const users = Users.find({}).fetch();
    return {
        users: users
    }
})(UsersList);

export default UsersListContainer;
