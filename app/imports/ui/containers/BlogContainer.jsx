import React from "react";
import { withTracker } from "meteor/react-meteor-data";
import Blog from "../pages/Blog.jsx";
import { Posts } from "../../api/posts/posts";

const BlogContainer = withTracker(props => {

    Meteor.subscribe('posts.blog');
    const posts = Posts.find({}).fetch();

    return {
        user: props.user,
        posts: posts
    };
})(Blog);

export default BlogContainer;
