import React from "react";
import { withTracker } from "meteor/react-meteor-data";
import SignUpForm from "../pages/session/SignUpForm";

const SignUpContainer = withTracker(props => {
    return {
        user: Meteor.user(),
    };
})(SignUpForm);

export default SignUpContainer;
