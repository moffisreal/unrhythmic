import React from "react";
import { withTracker } from "meteor/react-meteor-data";
import LoginForm from "../pages/session/LoginForm";

const LoginContainer = withTracker(props => {
    return {
        user: Meteor.user()
    };
})(LoginForm);

export default LoginContainer;
