import React from "react";
import { withTracker } from "meteor/react-meteor-data";
import { Posts } from "../../api/posts/posts";
import PostUpdateForm from "../pages/private/posts/PostUpdateForm";

const PostUpdateFormContainer = withTracker(props => {
    Meteor.subscribe('posts.blog.item', props.match.params.id);
    const post = Posts.find({}).fetch();

    return {
        loading: false,
        post: post
    };

})(PostUpdateForm);

export default PostUpdateFormContainer;
