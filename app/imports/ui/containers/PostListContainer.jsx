import React from "react";
import { withTracker } from "meteor/react-meteor-data";
import PostList from "../pages/private/posts/PostList.jsx";
import { Posts } from "../../api/posts/posts";

const PostListContainer = withTracker(props => {

    Meteor.subscribe('posts.admin.all');
    const posts = Posts.find({}).fetch();

    return {
        posts: posts
    };
})(PostList);

export default PostListContainer;
