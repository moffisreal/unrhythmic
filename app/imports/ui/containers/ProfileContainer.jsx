import React from "react";
import { withTracker } from "meteor/react-meteor-data";
import Profile from "../pages/private/Profile";

const ProfileContainer = withTracker(props => {

    let user = Meteor.user();

    return {
        user: user
    };
})(Profile);

export default ProfileContainer;
