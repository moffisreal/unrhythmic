import React from "react";
import { withTracker } from "meteor/react-meteor-data";
import PostView from "../pages/public/PostView.jsx";
import { Posts } from "../../api/posts/posts";

const HomeContainer = withTracker(props => {

    Meteor.subscribe('posts.blog.item', props.match.params.id);
    const post = Posts.find({}).fetch();

    return {
        loading: false,
        post: post
    };
})(PostView);

export default HomeContainer;
