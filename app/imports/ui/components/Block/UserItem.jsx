import React, { Component } from "react";
import Button from "../Form/Button";

export default class User extends Component {

    isAdmin = false;

    constructor(props) {
        super(props);
        this.isAdmin = Roles.userIsInRole(this.props.user._id, 'admin');
    };

    render() {
        // only set posts classname for bottom page posts
        return <div ref={el => this.el = el} className={
                    this.props.type === "list" ?
                        ("content-list") // show list items
                        : ''
                    }>
                <article className="user" id={"article"+this.props.i}>
                    <div className="user__name">
                        { this.isAdmin
                            ? <div> { this.props.user.username } - <i title="" className="fas fa-user-shield"></i></div>
                            : <div> { this.props.user.username } - <i title="" className="fas fa-user"></i></div>
                        }
                    </div>
                    <div className="management-buttons">
                        { this.isAdmin ?
                            <Button action="noadmin" element="user" itemId={ this.props.user._id }/>
                            : <Button action="admin" element="user" itemId={ this.props.user._id }/>
                        }
                    </div>
                </article>
            </div>
    }
}
