import { Link, withRouter } from "react-router-dom";
import React from "react";

import "./style/AuthButton.less";

const AuthButton = withRouter(
    ({ history }) =>
        !!Meteor.userId() ? (
            <section id="login">
                <div id="logout__button">
                    <a
                        onClick={() => {
                            Meteor.logout();
                            history.push("/");
                        }}
                    >
                        Sign out <i className="fas fa-sign-out-alt"></i>
                    </a>
                </div>
            </section>
        ) : (
            <section id="login">
                <div id="login__button">
                    <Link to="/login"> Login <i className="fab fa-facebook-square"></i></Link>
                </div>
            </section>
        )
);

export default AuthButton;
