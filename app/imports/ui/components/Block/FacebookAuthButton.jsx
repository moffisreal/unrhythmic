import React from "react";
import Button from "../Form/Button";
import Form from "../Form/Form";

import "./style/FacebookButton.less";

export default class FacebookAuthButton extends Form {

    constructor(props){
        super(props);
        this.setFacebookLoginForm.bind(this);
    };

    setFacebookLoginForm = (e) => {

        e.preventDefault();

        Meteor.loginWithFacebook({
            requestPermissions: [
                'public_profile',
                'email'
            ]
        }, (err) => {
            if (err) {
                console.log(err)
            }
        });
    };

    render() {
        return (
            <div id="facebook">
                <Button
                    id="facebook__button"
                    name="submit"
                    type="text"
                    text="Login facebook"
                    onClick={ this.setFacebookLoginForm }
                />
               <p>
                   In a hurry? Log in with Facebook.<br/>
                   Remember: We'll never post anything without your permission.
                   {/*Pressé ? Connecte toi avec facebook<br/>
                   Rappelle toi: Nous ne posterons rien sans ta permission !*/}
               </p>
            </div>
        );
    };
}
