import React from "react/react";

import Menu from "./Menu";
import TopMenu from "./TopMenu";

import './style/Header.less';
import {Link} from "react-router-dom";
import AuthButton from "./AuthButton";

const TOP_MENU_LOGIN_LINKS = {
    links: {

        profile: {
            name: 'My Unrhythmic',
            link: '/profile'
        },
        signUp: {
            name: 'Sign Up',
            link: '/sign-up'
        },
    }
};

const Header = () => (
    <header id="header">
        <div id="header__container">
            {/* <div id="menu__top__login">
                <ul >
                    { !!Meteor.userId()
                        ? (
                            <li className="menu__top__item no-section small-font">
                                <Link to={TOP_MENU_LOGIN_LINKS.links.profile.link}>
                                    {TOP_MENU_LOGIN_LINKS.links.profile.name}
                                </Link>
                            </li>
                        ) : (
                            <li className="menu__top__item no-section small-font">
                                <Link to={TOP_MENU_LOGIN_LINKS.links.signUp.link}>
                                    {TOP_MENU_LOGIN_LINKS.links.signUp.name}
                                </Link>
                            </li>
                        )
                    }
                    <li className="menu__top__item small-font">
                        <AuthButton/>
                    </li>
                </ul>
            </div> */}
            <TopMenu/>
            <Menu/>
        </div>
    </header>
);

export default Header;

