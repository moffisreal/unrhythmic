import React from "react/react";
import Button from "../Form/Button";
import "./style/Footer.less";
import { Link } from "react-router-dom";

const Footer = () => (
    <footer id="footer">
        <div id="footer__div">
            <section id="social-buttons">
                <Button action="soundcloud" url="https://www.soundcloud.com/unrhythmic/"/>
                <Button action="facebook" url="https://www.facebook.com/unrhythmic/"/>
                <Button action="instagram" url="https://www.instagram.com/unrhythmic/"/>
                <Button action="mail"/>
            </section>
            <section id="footer__partners">
                <div>
                    <div className="footer__image__container footer__image__container__left">
                        <a href="https://www.residentadvisor.net/promoter.aspx?id=88597" target="_blank">
                            <img className="ra" src="/img/partners/ra.png" alt="resident-advisor"/>
                        </a>
                    </div>
                    <div className="footer__image__container">
                        <a href="https://subbacultcha.be/events/event/armless-kid-tom-nate-b2b-bee-fezz-d-nite" target="_blank">
                            <img className="subbacultcha" src="/img/partners/subbacultcha.png" alt="subculture"/>
                        </a>
                    </div>
                </div>
            </section>
            <section id="footer__info">
                Stay tuned! - © unrhythmic 2021 - <span>info[@]unrhythmic.be</span>
                <div id="privacy-policy" >
                    <Link to="/privacy-policy">
                        Privacy policy
                    </Link>
                </div><br/><br/>
                <p className="small">Web design by <span id="un-studio">unrhythmic™</span> studio</p>
            </section>
        </div>
    </footer>
);

export default Footer;
