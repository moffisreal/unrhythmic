import React, { Component } from "react/react";

import "./style/PopUp.less";
import Button from "../Form/Button";
import Success from "../../components/Form/Success";
import Error from "../../components/Form/Error";

export default class PopUp extends Component {

    title = "";
    confirmMethod;

    constructor(props) {
        super(props);

        this.state = {
            error: null,
            success: null
        };
        this.getConfirmMethod();
        this.getPopUpTitle();
    };

    getConfirmMethod() {
        let method;
        switch (this.props.action) {
            case "erase":
            case "delete":
            case "deleted":
                method = this.deletePost;
                break;
            case "noadmin":
            case "admin":
                method = this.setAdmin;
                break;
        }
        this.confirmMethod = method;
    }

    /* close popup */
    close = () => {
        this.props.unMount();
    };

    /* confirm deletion */
    deletePost = () => {
        (this.props.action === "delete" || this.props.action === "deleted")
            ? Meteor.call("posts.delete", {
                id: this.props.id
            }, (error, result) => {
                this.setStateFromMeteorCall(error, result);
            })
            : Meteor.call("posts.erase", {
                id: this.props.id
            }, (error, result) => {
                this.setStateFromMeteorCall(error, result);
            });
    };

    /* confirm admin */
    setAdmin = () => {
        Meteor.call("users.toggleAdmin", {
            id: this.props.id
        }, (error, result) => {
            this.setStateFromMeteorCall(error, result);
        });

        // workaround
        this.location.reload();
    };

    /* handle response */
    setStateFromMeteorCall = (error, result) => {
        let deleteActions = ["delete", "deleted", "erase"];
        let userActions = ["admin", "noadmin"];
        let success;

        if (deleteActions.includes(this.props.action)) {
             success = `Element successfully ${
                 this.props.action === "erase"
                     ? 'erased'
                     : ( this.props.action === 'delete' ? 'deleted' : 'restored' )
                }`
        } else if (userActions.includes(this.props.action)) {
            success = `User successfully ${
                this.props.action === "admin"
                    ? 'set to admin!'
                    : 'removed from admins!'
                }`
        }

        this.setState({
            error: error === 0 ? "Error while modifying this element" : null,
            success: result === 1 ? success : null
        });
    };

    getPopUpTitle = () => {
        let className = this.props.element;
        let action = this.props.action;

        switch ( className ) {
            case "post":
                if ( action === "delete" ) {
                    this.title = "Do you confirm news deletion?"
                }
                else if ( action ===  "deleted" ) {
                    this.title = "Do you confirm news restore?"
                }
                else if ( action ===  "erase" ) {
                    this.title = "Do you confirm news definitive REMOVAL (can not be undone) ?"
                }
                break;
            case "user":
                if ( action === "admin" ) {
                    this.title = "Do you confirm admin add?"
                } else if ( action === "noadmin" ) {
                    this.title = "Do you confirm admin removal?"
                }
                break;
        }
    };

    render() {
        return (
            <div className="popup__container">
                <div className="popup__content">
                    { this.state.success !== null || this.state.error !== null ?
                        (<div>
                            <Success type="top" success={ this.state.success  }/>
                            <Error type="top" error={ this.state.error  }/>
                            <Button text="Close" onClick={ this.close }/>
                        </div>)
                        : (<div>
                            <h2>{ this.title }</h2>
                            <Button text="No" onClick={ this.close }/>
                            <Button text="Yes" onClick={ this.confirmMethod }/>
                        </div>)
                    }
                </div>
            </div>
        );
    }


}
