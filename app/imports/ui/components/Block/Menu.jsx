import React, { Component } from "react";
import { Link } from "react-router-dom";

import './style/Menu.less';
import AuthButton from "./AuthButton";
import Button from "../Form/Button";

const MENU_LINKS = {
    links: {
        home: {
            name: 'Home',
            link: '/'
        },
        about: {
            name: 'About',
            link: '/about'
        },
        profile: {
            name: 'My Unrhythmic',
            link: '/profile'
        },
        ticket: {
            name: 'Tickets',
            link: 'https://www.residentadvisor.net/events/1285909'
        },
        blog: {
            name: 'Podcasts',
            link: '/podcasts'
        },
        signUp: {
            name: 'Sign Up',
            link: '/sign-up'
        },
        management :{
            name: 'Admin',
            link: '/management'
        }
    }
};

class Menu extends Component {

    constructor(props) {
        super(props);
        Menu.mobileMenuAction.bind(this);
    };

    static mobileMenuAction() {
        let menu = $("#menu");
        let menuTop = $("#menu__top__login");
        if (menu.hasClass("visible")) {
            menu.removeClass("visible");
            menuTop.css('display', 'none');
        }
        else{
            menu.addClass("visible");
            menuTop.css('display', 'flex');
        }
    }

    render() {

        return <div id="menu" className={ !!Meteor.userId() && Roles.userIsInRole(Meteor.userId(), 'admin') ? 'menu-admin' : '' }>
            <ul>
                <li className="menu__item">
                    <Link to={MENU_LINKS.links.about.link}>
                        {MENU_LINKS.links.about.name}
                    </Link>
                </li>
                <li className="menu__item">
                    <Link to={MENU_LINKS.links.blog.link}>
                        {MENU_LINKS.links.blog.name}
                    </Link>
                </li>
                <li className="menu__item ticket-link">
                    {/*<Link to={MENU_LINKS.links.ticket.link}>*/}
                        {/*{MENU_LINKS.links.ticket.name}*/}
                    {/*</Link>*/}
                    <a href="https://www.residentadvisor.net/events/1388291" target="_blank">Tickets</a>
                </li>
                { !!Meteor.userId() && Roles.userIsInRole(Meteor.userId(), 'admin') ?
                    (<li className="menu__item admin">
                        <Link to={MENU_LINKS.links.management.link}>
                            {MENU_LINKS.links.management.name}
                        </Link>
                    </li>) : ''
                }
            </ul>
            <div id="social-buttons">
                <Button action="soundcloud" url="https://www.soundcloud.com/unrhythmic/"/>
                <Button action="facebook" url="https://www.facebook.com/unrhythmic/"/>
                <Button action="instagram" url="https://www.instagram.com/unrhythmic/"/>
                <Button action="mail"/>
            </div>
            <div id="showMenuAction" onClick={ Menu.mobileMenuAction } title="Click here to display menu">
                <i className="fas fa-bars"/>
            </div>
        </div>
    }
}

export default Menu;
