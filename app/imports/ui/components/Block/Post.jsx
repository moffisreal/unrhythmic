import React, { Component } from "react";
import { _ } from 'meteor/underscore';

import Util from "../Util/Util.js";
import Button from "../Form/Button";
import * as ReactDOM from "react-dom";

export default class Post extends Component {

    constructor(props) {
        super(props);
        this.content = null;
        this.parseContent.bind(this);
    };

    /**
     * Reformat content from string
     *
     * @param content String
     */
    parseContent = (content) => {
        // clean content from possible xss injection
        let string = _.unescape(content);
        // get html from string
        let html = $.parseHTML( string );
        // format article date
        let date = Util.formatDate(this.props.post.createdAt);
        // add date into the article
        html = Post.addDateInArticle(html, date);
        // get content html object with each elements of article
        this.content = $(html);

        // parse content
        this.content.each((index, element) => {

            // add useful classes for display
            switch(this.content[index].nodeName){
                case 'H2':
                    $(element).addClass("h2");
                    break;
                case 'H3':
                    $(element).addClass("h3");
                    break;
                case 'H4':
                    $(element).addClass("h4");
                    break;
                case 'H5':
                    $(element).addClass("h5");
                    break;
                case 'P':
                    $(element).addClass("p");
                    // if image is child, add it a class too
                    if (element.firstChild !== undefined && element.firstChild.nodeName === "IMG"){
                        let el = $(element.firstChild);
                        el.addClass("content-background-bottom__image");
                    }
                    break;
            }
        });

        return this.content;
    };

    static addDateInArticle( html, date )
    {
        let htmlDate = "<div class=\"txt-right\">" + date + "</div>";
        htmlDate = $(htmlDate);
        html.unshift(htmlDate[0]);

        return html;
    }

    componentDidMount() {

        let contentObj = this.parseContent(this.props.post.text);

        // limit content of article
        let content = [];
        $(contentObj).each((i,element) => {
            if (!this.props.showAll){
                if(this.props.type !== "list" && i < 4) {
                    content[i] =  $(element);
                }
                else if (this.props.type === "list" && i < 2){
                    content[i] =  $(element);
                }
            }
            else if (this.props.showAll === true){
                content[i] =  $(element);
            }
        });
        // append elements to dom
        $(ReactDOM.findDOMNode(this.el)).find("#article"+this.props.i).prepend(content);
    }

    render() {
        // only set posts classname for bottom page posts
        return <div ref={el => this.el = el} className={
                    this.props.type === "list" ?
                        ("content-list") // show list items
                        : this.props.type === "full" ?
                            ("content-full")
                            : ( !this.props.preview ?
                                    ( this.props.i !== 1 ? // difference between top article and bottom articles
                                        "content-bottom__article--even"
                                        : "content-background-top")
                                    : ( "" )
                        )
                    }>
                <article className="article" id={"article"+this.props.i}>
                    { !this.props.showAll && this.props.type !== "list" ?
                        <Button action="read-next" url={ "/podcasts/view/"+this.props.post._id }/>
                        : (
                            !this.props.showAll ?
                                (<div className="management-buttons">
                                <Button action="update" element="post" url={ "/management/podcasts/update/"+this.props.post._id }/>
                                <Button action="erase" element="post" itemId={ this.props.post._id }/>
                                {
                                    this.props.post.deletionTime === null ?
                                        <Button action="delete" element="post" itemId={ this.props.post._id }/>
                                        : <Button action="deleted" element="post" itemId={ this.props.post._id }/>
                                }
                                </div>)
                                : ( "" )
                        )
                    }
                </article>
            </div>
    }
}
