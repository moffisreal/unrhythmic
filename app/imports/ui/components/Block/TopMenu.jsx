import React, { Component } from "react";
import { Link } from "react-router-dom";

import './style/TopMenu.less';

const TOP_MENU_LINKS = {
    links: {
        home: {
            name: 'Home',
            link: '/'
        },
        about: {
            name: 'About',
            link: '/about'
        },
        ticket: {
            name: 'Tickets',
            link: 'https://www.residentadvisor.net/events/1285909'
        },
        blog: {
            name: 'Podcasts',
            link: '/podcasts'
        },
        management :{
            name: 'Admin',
            link: '/management'
        }
    }
};

class TopMenu extends Component {

    constructor(props) {
        super(props);
    };

    render() {

        return <div>
            <ul id="menu__top">
                <li className="menu__top__item no-section ticket-link">
                    {/*<Link className="menu__right__margin" to={TOP_MENU_LINKS.links.ticket.link}>*/}
                        {/*{TOP_MENU_LINKS.links.ticket.name}*/}
                    {/*</Link>*/}

                    <a href="https://www.residentadvisor.net/events/1388291" target="_blank">Tickets</a>
                </li>
                <li className="menu__top__item no-section">
                    <Link to={TOP_MENU_LINKS.links.blog.link}>
                        {TOP_MENU_LINKS.links.blog.name}
                    </Link>
                </li>
                <li className="menu__top__item no-section">
                    <Link to={TOP_MENU_LINKS.links.about.link}>
                        {TOP_MENU_LINKS.links.about.name}
                    </Link>
                </li>
                { !!Meteor.userId() && Roles.userIsInRole(Meteor.userId(), 'admin') ?
                    (<li className="menu__top__item admin no-section">

                        <Link to={TOP_MENU_LINKS.links.management.link}>
                            {TOP_MENU_LINKS.links.management.name}
                        </Link>
                    </li>) : ''
                }
            </ul>
        </div>
    }
}

export default TopMenu;
