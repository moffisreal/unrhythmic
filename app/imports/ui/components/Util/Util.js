export default class Util {
    constructor() {
    };

    /**
     *
     * @return string formatted String
     * @param date String
     */
    static formatDate(date){
        date = new Date(date);
        let options = { year: 'numeric', month: 'long', day: 'numeric' };
        return date.toLocaleDateString('en-EN', options);
    }
}
