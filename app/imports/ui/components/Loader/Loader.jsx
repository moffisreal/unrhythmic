import React from "react/react";

import "./style/Loader.less";

const Loader = () => (
    <div className="loader-app">
        { Meteor.loggingIn() ? (
              <img src="" alt="LOADING"/>
            ) : ("")
        }
    </div>
);

export default Loader;
