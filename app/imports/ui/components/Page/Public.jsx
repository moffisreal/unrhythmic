import React, { Component } from "react/react";
import Container from "../../layouts/Container.jsx";
import Header from "../Block/Header";

export default class Public extends Component {

    componentDidMount() {
        window.scrollTo(0, 0)
    }

    render() {
        return (
            <Container>
                <div className="container__div">
                    <section>
                        <Header/>
                        { this.props.children }
                    </section>
                </div>
            </Container>
        );
    }
}
