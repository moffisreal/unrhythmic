import React, { Component } from "react/react";

import "./Auth.less";
import Container from "../../layouts/Container.jsx";
import Header from "../Block/Header";

export default class Auth extends Component {

    componentDidMount() {
        window.scrollTo(0, 0)
    }

    render() {
        return (
            <Container>
                <div className="container__div">
                    <section>
                        <Header/>
                        { this.props.children }
                        <section className="extras">
                            { this.props.extras || null }
                        </section>
                    </section>
                </div>
            </Container>
        );
    }
}
