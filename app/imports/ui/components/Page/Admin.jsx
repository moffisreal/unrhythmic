import React, { Component } from "react/react";
import Container from "../../layouts/Container.jsx";
import { Redirect } from "react-router-dom";
import Header from "../Block/Header";

export default class Admin extends Component {

    componentDidMount() {
        window.scrollTo(0, 0)
    }

    render() {
        return (
            <Container>
                <div className="container__div">
                    { // necessary to wait for roles to be loaded.
                        (Roles.subscription.ready()) ? (
                            // check if user logged and if is admin
                            !Meteor.userId() || !Roles.userIsInRole( Meteor.userId(), 'admin' ) ?
                                ( <Redirect to="/"/> )
                                : (
                                    <section>
                                        <Header/>
                                        { this.props.children }
                                    </section>
                                )
                        ) : ( '' )
                    }
                </div>
            </Container>
        );
    }
}
