import React, { Component } from "react/react";

import './style/Error.less';

/**
 * @description Represents an Error
 * @extends Component
 */
export default class Error extends Component {

    class = '';

    constructor( props ) {
        super( props );
    }

    /**
     * @description Render an error
     * @returns {*}
     */
    render() {

        return (
            <div className={ this.props.error ?
                                (this.props.type !== undefined
                                    ? "error--active "
                                    : "form__error--active" )
                                : "form__error"
            }>
                <div className="arrow"></div>
                { this.props.error !== null ? this.props.error : ''}
            </div>
        );
    }
}
