import React, {Component} from "react";
import {_} from "meteor/underscore";
import Input from "./Input";
import {UniHtml} from 'meteor/vazco:universe-html-purifier';

export default class Form extends Component {

    /**
     * This static exists because i can not use state in child form component to
     * set state directly in form, because after Meter.calls, components are unmounted
     *
     * @type {null}
     */
    static error = null;
    // idem for success message
    static success = null;

    constructor(props) {
        super(props);
        this.state = {
            formErrors : null,
            error : null,
            success: null
        };
    };

    /**
     * @description Reset errors in component state
     * @param formErrors
     * @param error
     */
    resetErrors = ( formErrors = true) => {
        this.setState(() => ({
            formErrors: formErrors === true ? null : this.state.formErrors
        }));
        Form.error = null;
    };

    /**
     * @description
     * Receive form elements json and set potential validation errors message or remove old ones
     * @Param Object elements
     * @Param Object errors
     */
    static setFormElementsErrors = ( elements, errors ) => {
        _.map( elements, ( element ) =>
        {
            // if no errors, no need to set. If errors diff
            if (errors !== null && errors.error !== 403)
            {
                let error = _.findWhere( errors.details, { name : element.name } );
                if ( error !== undefined ) {
                    element.error = error.type;
                }
                else { // reset error field in element if no more form errors
                    element.error = null;
                }
            }
            else { // reset error field in element if no more form errors
                element.error = null;
            }
        });

        return elements;
    };

    /**
     * Render form elements
     * @param elements
     */
    static renderElements(elements)
    {
        // reset general form error
        Form.error = null;
        // map elements and render them
        return _.map(elements,
            ( num, key ) => {
                return <Input
                    ref={ num.name }
                    key={ key }
                    name={ num.name }
                    type={ num.type }
                    error={ num.error }
                    text={ num.text }
                />;
            }
        )
    }

    /**
     * Sanitize object containing form values
     * @param obj
     * @return object
     */
    static sanitizeValues(obj) {
        for (let element in obj) {
            if (obj.hasOwnProperty(element))
            {
                if (typeof obj[element] !== "object") {
                    obj[element] = UniHTML.purify(obj[element], {"encodeHtmlEntities": true});
                }
            }
        }
    };

    /**
     * Remove <script> from string to avoid xss injection
     * @param value
     */
    static removeScriptTag(value) {
        value = value.replace('&lt;script&gt;', '').replace('&lt;/script&gt;', '');
        return value.replace(/<script>/ig, '').replace(/<\/script>/ig, '');
    }

    /**
     * Return html tags encoded
     * @param str
     * @returns {*}
     */
    static encodeHtmlEntities(str) {
        return str.replace(/</g, '&lt;').replace(/>/g, '&gt;');
    }

    /**
     * Render a general form error, not especially linked to a field.
     * It is not a 'field validation' error.
     * @returns {*}
     */
    render() {
        return (
            <div>
            </div>
        );
    };
}
