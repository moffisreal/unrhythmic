import React, { Component } from "react/react";
import Error from "./Error";

import './style/Input.less';

/**
 * @description Represents a form input
 * @extends Component
 */
export default class Input extends Component {

    constructor(props) {
        super(props);
    }

    /**
     * @description Render an Input with label and Error
     * @returns {*}
     */
    render() {
        return (
            <div className="form__input">
                <label htmlFor={ this.props.name }> { this.props.text } </label>
                <input type={ this.props.type } name={ this.props.name } ref={ this.props.name }/>
                <Error error={ this.props.error } />
            </div>
        );
    }
}
