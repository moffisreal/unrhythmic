import React, { Component } from "react/react";

import {
    Link, Redirect
} from "react-router-dom";

import './style/Button.less';
import PopUp from "../Block/PopUp";

export default class Button extends Component {

    constructor(props) {
        super(props);

        this.setRedirect.bind(this);
        this.renderRedirect.bind(this);
        this.setClosePopUp.bind(this);

        this.state = {
            redirect: false,
        }
    };

    getClassName(){
        let className;
        if (this.props.action !== null && this.props.action !== undefined){
            className = "management-button"
        } else {
            switch (this.props.id ) {
                case "facebook__button":
                    className = "";
                    break;
                case "redirect__button":
                    className = "redirect__button";
                    break;
                default:
                    className = "form__button button";
                    break;
            }
        }
        return className;
    };

   setRedirect = () => {
        this.setState({
            redirect: true
        });
    };

   setClosePopUp = () => {
       this.setState({
           redirect: false
       });
   };

   renderRedirect = () => {

       if (this.state.redirect) {
           switch (this.props.action) {
               case "erase":
               case "delete":
               case "deleted":
               case "noadmin":
               case "admin":
                   return <PopUp unMount={ this.setClosePopUp } action={ this.props.action } element={ this.props.element } id={ this.props.itemId } />;
                   break;
               case "update" :
                   return <Redirect to={ this.props.url }/>;
                   break;
               case "facebook" :
               case "instagram" :
               case "soundcloud" :
                   window.open( this.props.url );
                   break;
           }
       }
   };

    render() {
        let className = this.getClassName();
        return (
            <div>
                { this.props.action !== "read-next" ?
                    <div>
                        <button
                            className={ className }
                            id={ this.props.id }
                            type={ this.props.type }
                            name={ this.props.name }
                            onClick={ !this.props.onClick ? this.setRedirect : this.props.onClick }>
                            { this.props.text }
                            { this.props.id === "facebook__button" ? <i className="fab fa-facebook-square"></i> : '' }
                            {
                                /* management actions */
                                this.props.action === "erase" ?
                                    <i className="fas fa-ban"></i>
                                    : (this.props.action === "delete" || this.props.action === "deleted" ?
                                        (
                                            this.props.action === "deleted" ?
                                                <i className="fas fa-trash-restore"></i>
                                                : <i className="far fa-trash-alt"></i>
                                        )
                                        :
                                        (
                                            this.props.action === "update" ?
                                                <i className="far fa-edit"></i>
                                                : ""
                                        )
                                    )
                            }
                            {
                                /* admin setting */
                                this.props.action === 'admin' || this.props.action === 'noadmin' ?
                                    (this.props.action === 'noadmin' ?
                                        "Set user"
                                        : "Set admin"
                                    ) : ""
                            }
                            {
                                /* social stuff */
                                this.props.action === "facebook"
                                || this.props.action === "instagram"
                                || this.props.action === "mail"
                                || this.props.action === "soundcloud" ?
                                    (
                                        this.props.action === "facebook" ?
                                            <i className="fab fa-facebook-f square" title="facebook"></i>
                                            : this.props.action === "instagram" ?
                                                <i className="fab fa-instagram" title="instagram"></i>
                                                :  this.props.action === "soundcloud" ?
                                                    <i className="fab fa-soundcloud"></i>
                                                    : (<a href="mailto:info@unrhythmic.be">
                                                        <i className="fas fa-paper-plane" title="mail"></i>
                                                         </a>)
                                    )
                                    : ""
                            }
                        </button>
                        {
                            this.renderRedirect()
                        }
                    </div>
                    :
                    <button
                        className="read-next__button"
                        id={ this.props.id }
                        type={ this.props.type }
                        name={ this.props.name }>
                        <Link className="read-next__a" to={ this.props.url }>
                            Read more
                        </Link>
                    </button>
                }
            </div>
        );
    }
}
