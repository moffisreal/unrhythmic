import React, { Component } from "react/react";

import './style/Success.less';

/**
 * @description Represents an Error
 * @extends Component
 */
export default class Success extends Component {

    class = '';

    constructor( props ) {
        super( props );
    }

    /**
     * @description Render an error
     * @returns {*}
     */
    render() {

        return (
            <div className={ this.props.success ?
                (this.props.type !== undefined
                    ? "success--active "
                    : "form__success--active" )
                : "form__success"
            }>
                <div className="arrow"></div>
                { this.props.success !== null ? this.props.success : ''}
            </div>
        );
    }
}
