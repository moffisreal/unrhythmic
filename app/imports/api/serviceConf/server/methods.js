import { DDPRateLimiter } from "meteor/ddp-rate-limiter";
import { _ } from "meteor/underscore";
import { ValidatedMethod } from "meteor/mdg:validated-method";

export const setConf = new ValidatedMethod({
    name: 'facebook.setConf',
    validate(){ },
    run() {
        ServiceConfiguration.configurations.remove({
            service: "facebook"
        });
        ServiceConfiguration.configurations.upsert(
            {service: 'facebook'},
            {
                $set: {
                    redirectUrl: "/",
                    loginStyle: "redirect",
                    appId: Meteor.settings.facebook.appId,
                    secret: Meteor.settings.facebook.secret
                }
            }
        );
    },
});

// Get list of all method names on Users
const FACEBOOK_METHODS = _.pluck([
    setConf
], 'name');

DDPRateLimiter.addRule({
    name(name) {
        return _.contains(FACEBOOK_METHODS, name);
    },

    // Rate limit per connection ID
    connectionId() { return true; },
}, 5, 1000);

