import { Meteor } from 'meteor/meteor';
import { DDPRateLimiter } from 'meteor/ddp-rate-limiter';
import { _ } from 'meteor/underscore';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import { Posts } from "./../posts";

/**
 * Validate post delete
 * @type {ValidatedMethod}
 */
Meteor.methods({
    'posts.delete'(idObject) {
        new SimpleSchema({
            id: {
                type: String,
                custom: function ()
                {
                    if (this.value.length < 1) {
                        return "wrongIdException";
                    }
                }
            }
        }).validator(idObject);

        if (!this.userId && !Roles.userIsInRole( Meteor.userId(), 'admin' )) {
            throw new Meteor.Error('not-authorized');
        }

        let posts = Posts.find({
            _id: idObject.id,
            deletionTime: { $eq : null }
        }).fetch();

        return Posts.update({_id: idObject.id},
            { $set:
                    {
                        deletionTime: posts[0] !== undefined ? new Date() : null
                    }
            }
        )
    },
    'posts.erase'(idObject) {
        new SimpleSchema({
            id: {
                type: String,
                custom: function ()
                {
                    if (this.value.length < 1) {
                        return "wrongIdException";
                    }
                }
            }
        }).validator(idObject);

        if (!this.userId && !Roles.userIsInRole( Meteor.userId(), 'admin' )) {
            throw new Meteor.Error('not-authorized');
        }
        return Posts.remove({_id: idObject.id});
    },
    'posts.insert'(post) {
        new SimpleSchema({
            text: {
                type: String,
                custom: function ()
                {
                    if (this.value.length < 1) {
                        return "Text Too Short";
                    }
                }
            },
            delta: {
                type: String,
                custom: function ()
                {
                    if (this.value.length < 1) {
                        return "Delta fucked up";
                    }
                }
            }
        }).validator(post);

        if (!this.userId && !Roles.userIsInRole( Meteor.userId(), 'admin' )) {
            throw new Meteor.Error('not-authorized');
        }

        Posts.insert({
            text: post.text,
            delta: post.delta,
            createdAt: new Date(),
            owner: this.userId,
            username: Meteor.users.findOne(this.userId).username,
            deletionTime: null
        });
    },
    'posts.update'(post) {
        new SimpleSchema({
            text: {
                type: String,
                custom: function ()
                {
                    if (this.value.length < 1) {
                        return "Text too short";
                    }
                }
            },
            delta: {
                type: String,
                custom: function ()
                {
                    if (this.value.length < 1) {
                        return "delta fucked up";
                    }
                }
            },
            id: {
                type: String
            }
        }).validator(post);

        if (!this.userId && !Roles.userIsInRole( Meteor.userId(), 'admin' )) {
            throw new Meteor.Error('not-authorized');
        }

        return Posts.update({_id: post.id},
            { $set:
                    {
                        delta: post.delta,
                        text: post.text
                    }
            }
        )
    }
});

// Get list of all method names on Users
const POSTS_METHODS = _.pluck([
    "posts.delete",
    "posts.erase",
    "posts.insert",
    "posts.update",
], 'name');

if (Meteor.isServer) {
    // Only allow 5 create user operations per connection per second
    DDPRateLimiter.addRule({
        name(name) {
            return _.contains(POSTS_METHODS, name);
        },

        // Rate limit per connection ID
        connectionId() { return true; },
    }, 5, 1000);
}
