import { Meteor } from 'meteor/meteor';
import { Posts } from '../posts.js';

Meteor.publish('posts.admin.all', function () {
    return Posts.find(
        {},
        {
            sort: { createdAt: -1 },
        });

    // if user admin is no more admin, subscription will not re run : see "publish composite" https://guide.meteor.com/data-loading.html
    // it is a NICE TO HAVE but really useless now
});

Meteor.publish('posts.blog', function () {
    return Posts.find(
        {
            deletionTime : { $eq: null },
        },
        {
            sort: { createdAt: -1 },
            limit: 10
        });
});

Meteor.publish('posts.blog.item', function (postId) {
    return Posts.find(
        {
            deletionTime : { $eq: null },
            _id : { $eq: postId }
        });
});
