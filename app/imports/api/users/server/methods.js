import { DDPRateLimiter } from 'meteor/ddp-rate-limiter';
import { _ } from 'meteor/underscore';
import { Accounts } from 'meteor/accounts-base';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import { Meteor } from "meteor/meteor";

/**
 * Validate user insert
 * @type {ValidatedMethod}
 */
Meteor.methods({
    'users.insert'(userProfile) {
        new SimpleSchema({
            email: {
                type: String,
                custom: function ()
                {
                    let regEx = SimpleSchema.RegEx.Email;
                    if (!regEx.test(this.value)) {
                        return "Email is invalid"
                    }
                }
            },
            username : {
                type: String,
                custom: function () {
                    let valueLength = this.value.length;
                    if (valueLength < 6) {
                        return "Username must be at least 6 character long";
                    }
                }
            },
            password : {
                type: String,
                custom: function () {
                    // regex means : min 8 char with 1 special char, 1 uppercase char and 1 number.
                    let regEx = new RegExp("(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#%^*/\\-+;:,])(?=.{8,})","g");
                    if (!regEx.test(this.value)) {
                        return "Password is too weak";
                    }
                }
            },
            passwordRepeat : {
                type: String,
                custom: function() {
                    if (this.value !== this.field('password').value) {
                        return "Password does not match";
                    }
                },
            },
            profile: {
                type: Object
            },
            // nested object key access annotation
            "profile.name.first": {
                type: String
            },
            "profile.name.last": {
                type: String
            }
        }).validate(userProfile);

        /*
        * BACKUP SET MYSELF AS ADMIN DEV
        *
            let id = Accounts.createUser(userProfile);
            Roles.addUsersToRoles(id, "admin");
            return id;
        */

        // create user account if form is valid
        return Accounts.createUser(userProfile);
    },
    'users.firstUserAdmin'() {
        if (Meteor.users.find().count() <= 0) {
            let adminId = Accounts.createUser({
                username: Meteor.settings.admin.username,
                email: "damien.bouffioux@gmail.com",
                password: Meteor.settings.admin.password
            });
            Roles.addUsersToRoles(adminId, "admin");
        }
    },
    'users.toggleAdmin'({id}) {
        if (!this.userId && !Roles.userIsInRole( Meteor.userId(), 'admin' )) {
            throw new Meteor.Error('not-authorized');
        } else {
            Roles.userIsInRole(id, "admin") ?
                Roles.removeUsersFromRoles(id, "admin")
                : Roles.addUsersToRoles(id, "admin");
            return 1;
        }
    },
    'users.resetPassword'(passwords){
        new SimpleSchema({
            newPassword : {
                type: String,
                custom: function () {
                    // regex means : min 8 char with 1 special char, 1 uppercase char and 1 number.
                    let regEx = new RegExp("(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#%^*/\\-+;:,])(?=.{8,})","g");
                    if (!regEx.test(this.value)) {
                        return "Password is too weak";
                    }
                }
            },
            passwordRepeat : {
                type: String,
                custom: function() {
                    if (this.value !== this.field('newPassword').value) {
                        return "Password does not match";
                    }
                },
            }
        }).validate(passwords);
    }
});

// Get list of all method names on Users
const USERS_METHODS = _.pluck([
    "users.insert",
    "users.firstUserAdmin",
    "users.resetPassword",
    "users.toggleAdmin"
], 'name');

DDPRateLimiter.addRule({
    name(name) {
        return _.contains(USERS_METHODS, name);
    },

    // Rate limit per connection ID
    connectionId() { return true; },
}, 1, 1000);
