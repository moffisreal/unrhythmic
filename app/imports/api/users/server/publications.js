import { Meteor } from 'meteor/meteor';
import { Users } from '../users.js';

Meteor.publish('users.myUser', function () {

    if (!this.userId) {
        return this.ready();
    }

    const userId = this.userId;

    const selector = {
        _id : { $in, userId}
    };

    const options = {
        fields: {
            username: 1,
            emails: 1,
            profile: 1
        }
    };

    return Users.find(selector, options);
});

Meteor.publish('users.list', function (){
    if (Roles.userIsInRole(Meteor.user(),'admin')){
        return Meteor.users.find({});
    } else {
        this.stop();
    }
});


