// Register your apis here

import '../../api/users/server/methods.js';
import '../../api/users/server/publications.js';

import '../../api/serviceConf/server/methods.js';

import '../../api/posts/server/publications.js';
import '../../api/posts/server/methods.js';
