import React, { Component } from "react";
import { withTracker } from 'meteor/react-meteor-data';
import {
    Router,
    Route,
    Switch,
    Redirect
} from "react-router-dom";

import BlogContainer from '../../ui/containers/BlogContainer';
import LoginContainer from "../../ui/containers/LoginContainer";
import SignUpContainer from "../../ui/containers/SignUpContainer";
import PostListContainer from "../../ui/containers/PostListContainer";
import PostUpdateFormContainer from "../../ui/containers/PostUpdateFormContainer";
import PostViewContainer from "../../ui/containers/PostViewContainer";
import ProfileContainer from "../../ui/containers/ProfileContainer";

import NoMatch from '../../ui/pages/routing/NoMatch';
import PrivacyPolicy from "../../ui/pages/policy/PrivacyPolicy";
import Management from "../../ui/pages/private/Management";
import PostCreateForm from "../../ui/pages/private/posts/PostCreateForm";
import Home from "../../ui/pages/Home";
import UsersListContainer from "../../ui/containers/UsersListContainer";
import About from "../../ui/pages/public/About";

const browserHistory = require("history").createBrowserHistory();

/**
 * @description Define application routes
 * @extends Component
 */
export class Routes extends Component {

    render() {
        return <Router history={ browserHistory }>
            <div>
                {/* Routes definition */}
                <Switch>
                    /* Public menu routes */
                    <Route exact path="/" component={ Home } />
                    <Route exact path="/login" component={ props => <LoginContainer {...props}/> } />
                    <Route exact path="/sign-up" component={ props => <SignUpContainer {...props}/> } />
                    <Route exact path="/privacy-policy" component={ PrivacyPolicy } />
                    <Route exact path="/podcasts" component={ props => <BlogContainer {...props}/> }/>
                    <Route exact path="/podcasts/view/:id" component={ props => <PostViewContainer {...props}/> } />
                    {/*<Route exact path="/ticket" component={ props => <Ticket {...props}/> }/>*/}
                    <Route exact path="/about" component={ props => <About {...props}/>} />
                    {/*<Route exact path="/artist" component={ props => <BlogContainer {...props}/> }/>*/}

                    /* Private menu routes */
                    <PrivateRoute exact path="/profile" component={ props => <ProfileContainer {...props}/> } />

                    /* Private admin privileged routes */
                    <PrivateRoute exact path="/management" component={ props => <Management {...props}/> } />
                        /* post */
                        <PrivateRoute exact path="/management/create-post" component={ props => <PostCreateForm {...props}/> } />
                        <PrivateRoute exact path="/management/podcasts-list" component={ props => <PostListContainer {...props}/> } />
                        <PrivateRoute exact path="/management/podcasts/update/:id" component={ props => <PostUpdateFormContainer {...props}/> } />
                        /* users */
                        <PrivateRoute exact path="/management/users-list" component={ props => <UsersListContainer {...props}/> } />

                    /* Default no match route (404) */
                    <Route component={ props => <NoMatch {...props}/> }/>
                </Switch>
            </div>
        </Router>
    }
}

/**
 * @description Create a private route, must log in to access
 * @param Component
 * @param rest
 * @returns {*}
 * @constructor
 */
function PrivateRoute({ component: Component, ...rest }) {
    return <Route
        { ...rest }
        render={ props =>
            !!Meteor.userId() ? (
                <Component { ...props } />
            ) : (
                <Redirect
                    to={{
                        pathname: "/login",
                        state: { from: props.location }
                    }}
                />
            )
        }
    />
}

export default withTracker(() => {
    return {
        user: Meteor.user(),
        loggingIn: Meteor.loggingIn()
    }
})(Routes)
