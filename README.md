# DEVELOPMENT #

1. Clone this repo
2. Run `npm install` to install needed meteor app dependencies.
3. Run `meteor --settings settings.json` to recreate .meteor/local folder and local files. 
4. Clone `nginx-proxy` docker image on this repo. You may see network missing error for docker, juste recreate the network as docker tells you.
5. Launch `docker-compose up -d` on nginx-proxy project root folder.
6. Launch `docker-compose -f docker-compose.dev.yml up -d --build` in the unrhythmic project root folder (where docker-compose.dev.yml file is).
7. Pray & wait. You should even add `127.0.0.1  unrhythmic.local` to your etc/hosts file and connect to the app from https://unrhythmic.local 


# PRODUCTION #

# DEPLOY TECHNIQUE #

0. `git pull && meteor npm install` 
1. `sudo docker service restart`
2. `sudo docker-compose down`
3. `sudo docker-compose up -d --build`

# Certificate ssl #

-  Renew manually certificate

`chmod +x init-letsencrypt.sh`

then 

`sudo ./init-letsencrypt.sh`

- Initial procedure (automatic renew)
https://medium.com/@pentacent/nginx-and-lets-encrypt-with-docker-in-less-than-5-minutes-b4b8a60d3a71

Note : certificate should ne renewed automatically when it is about to expire. (?)

# Error while deploying app in production #

1. **ENOMEM :**

 had the same problem and as it turned out, my system had no swap space enabled. Check if this is the case by running the command `free -m`:

vagrant@vagrant-ubuntu-trusty-64:~$ free -m
             total       used       free     shared    buffers     cached
Mem:          2002        233       1769          0         24         91
-/+ buffers/cache:        116       1885
Swap:            0          0          0

Looking at the bottom row we can see we have a total of 0 bytes swap memory. Not good. Node can get pretty memory hungry and if no swap space is available when memory runs out, errors are bound to happen.

The method for adding a swap file varies between operating systems and distributions, but if you're running Ubuntu like me you can follow these instructions on adding a swap file:

`sudo fallocate -l 100M /swapfile` Create a 100 megabyte swapfile
`sudo chmod 600 /swapfile` Secure the swapfile by restricting access to root
`sudo mkswap /swapfile` Mark the file as a swap space
`sudo swapon /swapfile` Enable the swap
`echo "/swapfile none swap sw 0 0" | sudo tee -a /etc/fstab` Persist swapfile over reboots 

To remove the swap file :

At a shell prompt as root, execute the following command to disable the swap file (where /swapfile is the swap file):
`swapoff -v /swapfile`
Remove its entry from the `/etc/fstab file`.
Remove the actual file:
`rm /swapfile`



2. **ENOSPC :**

 Run : `du -hi` and check if sdb/dva1 is full
 Then : `sudo docker system prune`
 
 retry
 
 
 
3. **NO DB INITIALISED (script .sh failed) :**

error lines:
/usr/local/bin/docker-entrypoint.sh: running /docker-entrypoint-initdb.d/init.sh
/docker-entrypoint-initdb.d/init.sh: line 3: $'\r': command not found

LINUX: need to do `dos2unix <filename>`

WINDOWS: 
 check in editor that file is encoded as UTF-8 (no bom) AND with ending file line as "LN" not "CRLF" .. 

*OR* it can be because the running init.sh script for db is not runninf
because docker wont run it if the volume already exists !!!

`docker-compose down --rmi all -v`

4. **Release file for http://security.ubuntu.com/ubuntu/dists/bionic-security/InRelease is not valid yet**

restart docker 

5. **Cleaning up docker environement**

* close containers and volumes
`docker-compose down --rmi all -v`

* remove containers
`docker container rm $(docker container ls -a -q)`

* remove all images 
`docker image rm $(docker image ls -a -q)`

* clean the rest (volumes, dangling images, ..)
`docker system prune `

# Back up DB (mongo) #

Install mongodump and mongorestore clients

`sudo apt-get install mongodb-clients`

--- NEED TO UPGRADE SERVER FROM ubuntu 14.04.6 to 18.04! --- .....
